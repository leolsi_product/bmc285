//===================================================================
// File Name : includes.h
// Function  : 
// Program   : Boo-Ree Multimedia Inc.
// Date      : February, 04, 2013
// Version   : 0.01
// Mail      : support@boo-ree.com
// Web       : www.boo-ree.com
// History
//===================================================================

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <intrinsics.h>

#define NL  0x0A
#define CR  0x0D
#define BSP 0x08
#define ESC 0x1B

#define HZ		(1)
#define KHZ		(1000)
#define MHZ		(1000000)

 #define Fsys	(12 * MHZ)
// #define Fsys	(8 * MHZ)

#include "BMC285_REGDEF.h"
#if 0			// SFR union define.
typedef union {
	struct {
		volatile unsigned SW_STROBE_DATA	: 2;
		volatile unsigned TAMATCH_REM_ON	: 1;
		volatile unsigned REM_STAT		: 1;
		volatile unsigned TA_ENVELOPE		: 1;
		volatile unsigned CARRIER_CON		: 1;
	};
	volatile uint8_t reg;
} CA_CON1_t, *pCA_CON1_t;

typedef union {
	struct {
		volatile unsigned OSP		: 1;
		volatile unsigned MODE		: 1;
		volatile unsigned START		: 1;
		volatile unsigned STOP		: 1;
		volatile unsigned INTEN		: 1;
		volatile unsigned INTTIME	: 2;
		volatile unsigned CLKSEL		: 1;
	};
	volatile uint8_t reg;
} CA_CON0_t, *pCA_CON0_t;
#endif

// Hidden Register.
SFR(0xAD, rTRIVC);		// 
SFR(0xAE, rTRIM);		// 
SFR(0xAF, rTRISPROT);	// 

// 16-bit Access register
SFR_WORD(0xB3, rCADATAL16);	// CAFG Leading Data Low Register
SFR_WORD(0xB5, rCADATAT16);	// CAFG Trailing Data Low Register
SFR_WORD(0xC3, rT0DATA16);
SFR_WORD(0xC5, rT0PDR16);
SFR_WORD(0xD3, rT2DATA16);
SFR_WORD(0xD5, rT2PDR16);
SFR_WORD(0xF9, rU0CKDIV16);
SFR_WORD(0xFE, rSPIDATA16);
SFR16_WORD(0xFFAA, rXRAMADDR16);

#include "main.h"
#include "SysInit.h"
#include "System.h"

