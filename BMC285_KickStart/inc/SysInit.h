//===================================================================
// File Name : SysInit.h
// Function  : System Initialization
// Program   : Boo-Ree Multimedia Inc.
// Date      : February, 04, 2013
// Version   : 0.01
// Mail      : support@boo-ree.com
// Web       : www.boo-ree.com
// History
//===================================================================


#ifndef __SYSINIT_h__
#define __SYSINIT_h__

#if (Fsys == 12000000)
 #define BAUD_1200  	0x9C40
 #define IBW_1200		9
 #define BAUD_2400  	0x9C40
 #define IBW_2400		8
 #define BAUD_4800  	0x9C40
 #define IBW_4800		7
 #define BAUD_9600  	0x9C40
 #define IBW_9600		6
 #define BAUD_14400  	0xD055
 #define IBW_14400		5
 #define BAUD_19200  	0x9C40
 #define IBW_19200		5
 #define BAUD_38400  	0x9C40
 #define IBW_38400		4
 #define BAUD_57600  	0xD055
 #define IBW_57600		3
 #define BAUD_115200  	0xD055
 #define IBW_115200		2
// #define BAUD_230400  	x
// #define BAUD_460800  	x
// #define BAUD_921600  	x
#elif (Fsys == 8000000)		// real IMCLK : 6.85Mhz
 #define BAUD_57600  	(0x42AA)
 #define IBW_57600		(4)
#endif

#define LED0				(1<<0)
#define LED0_PORT			(rP6_b4)
#define LED1				(1<<1)
#define LED1_PORT			(rP6_b5)

/* variable type definitions */

#ifdef __SYSINIT_c__
#define SYSINIT_EXTERN
/** local definitions **/

/** internal functions **/

#else	/* __SYSINIT_c__ */
#define SYSINIT_EXTERN			extern
#endif	/* __SYSINIT_c__ */

/** global variable define **/

/** external functions **/
SYSINIT_EXTERN __near_func void System_Init(void);
SYSINIT_EXTERN __near_func void Console_Init(uint16_t byBaud, uint8_t ibw);
SYSINIT_EXTERN __near_func void GPIO_Init(void);
SYSINIT_EXTERN __near_func void SystemInitialization(void);

#endif	/* __SYSINIT_h__ */

