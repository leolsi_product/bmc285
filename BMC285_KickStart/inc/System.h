//===================================================================
// File Name : System.h
// Function  : System
// Program   : Boo-Ree Multimedia Inc.
// Date      : February, 04, 2013
// Version   : 0.01
// Mail      : support@boo-ree.com
// Web       : www.boo-ree.com
// History
//===================================================================

#ifndef __SYSTEM_h__
#define __SYSTEM_h__

/* variable type definitions */

#ifdef __SYSTEM_c__
#define SYSTEM_EXTERN
/** local definitions **/

/** internal functions **/

#else	/* __SYSTEM_c__ */
#define SYSTEM_EXTERN			extern
#endif	/* __SYSTEM_c__ */

/** global variable define **/

/** external functions **/
SYSTEM_EXTERN __near_func void LED_TOGGLE(uint8_t led);
SYSTEM_EXTERN __near_func void LED_ON(uint8_t led);
SYSTEM_EXTERN __near_func void LED_OFF(uint8_t led);

SYSTEM_EXTERN __near_func void _10udelay(uint8_t us);
SYSTEM_EXTERN __near_func void mdelay(uint16_t ms);
SYSTEM_EXTERN __near_func void delay(uint8_t sec);
SYSTEM_EXTERN int getchar(void);
SYSTEM_EXTERN int putchar(int c);
SYSTEM_EXTERN void putch(int c);
SYSTEM_EXTERN int getkey(void);
//SYSTEM_EXTERN int puts(const char *s);
SYSTEM_EXTERN void putstr(const char __code* str);
SYSTEM_EXTERN void puthex(uint8_t val, uint8_t cr);
SYSTEM_EXTERN void puthex16(uint16_t val, uint8_t cr);
SYSTEM_EXTERN void puthex32(uint32_t val, uint8_t cr);
SYSTEM_EXTERN void putdigit(uint8_t val, uint8_t cr);
SYSTEM_EXTERN void putint(uint8_t val, uint8_t cr);
SYSTEM_EXTERN void putint16(uint16_t val, uint8_t cr);
SYSTEM_EXTERN void putint32(uint32_t val, uint8_t cr);
SYSTEM_EXTERN void dump(uint8_t *ptr, uint8_t size);

#endif	/* __SYSTEM_h__ */





