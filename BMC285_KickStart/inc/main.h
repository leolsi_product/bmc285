//===================================================================
// File Name : main.h
// Function  : main program header
// Program   : Boo-Ree Multimedia Inc.
// Date      : February, 04, 2013
// Version   : 0.01
// Mail      : support@boo-ree.com
// Web       : www.boo-ree.com
// History
//===================================================================

#ifndef __MAIN_h__
#define __MAIN_h__

#define TEST_STANDBY

//#define DBGOUT
#define DBGPORT	0


/* variable type definitions */

#ifdef __MAIN_c__
#define MAIN_EXTERN
/** local definitions **/

/** internal functions **/

#else	/* __MAIN_c__ */
#define MAIN_EXTERN			extern
#endif	/* __MAIN_c__ */

/** external functions **/
MAIN_EXTERN __monitor uint8_t check_tick(uint8_t task_tick);
MAIN_EXTERN __task void main(void);

/** global variable define **/
MAIN_EXTERN volatile uint8_t tick_1ms;


#endif	/* __MAIN_h__ */

