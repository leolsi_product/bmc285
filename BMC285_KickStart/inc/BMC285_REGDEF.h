//===================================================================
// File Name : BMC285_REGDEF.h
// Function  : 
// Program   :   --  Boo-Ree Multimedia Inc.
// Date      : February, 04, 2013
// Version   : 0.01
// Mail      : support@boo-ree.com
// Web       : www.boo-ree.com
// History
//===================================================================

#ifndef __BMC285_REGDEF_h__
#define __BMC285_REGDEF_h__

#define __IAR_SYSTEM_ICC__
#ifdef __IAR_SYSTEM_ICC__	// IAR 8051 Compiler
//============================================================================
//      Compiler Dependent Definitions
//============================================================================
#if defined SDCC			// SDCC - Small Device C Compiler
	// ---------------------------------------------------------------------------
	#define SBIT(_ADDR,	_NAME)	__sbit  __at _ADDR	_NAME
	#define SFR(_ADDR,	_NAME)	__sfr   __at _ADDR	_NAME
	#define SFR16(_ADDR,_NAME)	__no_init volatile unsigned char _NAME @ _ADDR

	#define SFR_B_BITS(_ADDR, _NAME, _A,_B,_C,_D,_E,_F,_G,_H) \
		SFR(_ADDR,	_NAME); 			 \
		SBIT(_ADDR+0, _NAME ## _ ## _A); \
		SBIT(_ADDR+1, _NAME ## _ ## _B); \
		SBIT(_ADDR+2, _NAME ## _ ## _C); \
		SBIT(_ADDR+3, _NAME ## _ ## _D); \
		SBIT(_ADDR+4, _NAME ## _ ## _E); \
		SBIT(_ADDR+5, _NAME ## _ ## _F); \
		SBIT(_ADDR+6, _NAME ## _ ## _G); \
		SBIT(_ADDR+7, _NAME ## _ ## _H)

	#define SFR_B(_ADDR, _NAME) 	SFR_B_BITS(_ADDR, _NAME, b0, b1, b2, b3, b4, b5, b6, b7)
	// ---------------------------------------------------------------------------

#elif defined __ICC8051__	// IAR 8051
	// ---------------------------------------------------------------------------
	#include <stdbool.h>
	#define SFR(_ADDR,	_NAME)	__sfr	__no_init volatile unsigned char _NAME	@ _ADDR
	#define SFR_WORD(_ADDR,	_NAME)	__sfr	__no_init volatile unsigned short _NAME	@ _ADDR
	#define SFR16(_ADDR,_NAME)	__no_init __xdata volatile unsigned char _NAME @ _ADDR
	#define SFR16_WORD(_ADDR,_NAME) 		\
		__no_init volatile union {			\
			unsigned short _NAME;			\
			struct {						\
				unsigned char _NAME ## _L;	\
				unsigned char _NAME ## _H;	\
			};								\
		} @ _ADDR

	#define SFR_B_BITS(_ADDR, _NAME, _A,_B,_C,_D,_E,_F,_G,_H) \
	__sfr __no_init volatile union  {     	  \
		unsigned char   _NAME;                \
		struct 	{                         	  \
			unsigned char _NAME ## _ ## _A:1, \
			              _NAME ## _ ## _B:1, \
			              _NAME ## _ ## _C:1, \
			              _NAME ## _ ## _D:1, \
			              _NAME ## _ ## _E:1, \
			              _NAME ## _ ## _F:1, \
			              _NAME ## _ ## _G:1, \
			              _NAME ## _ ## _H:1; \
   		};  								  \
	} @ _ADDR


	#define SFR_B(_ADDR, _NAME) 	SFR_B_BITS(_ADDR, _NAME, b0, b1, b2, b3, b4, b5, b6, b7)
	// ---------------------------------------------------------------------------
#endif

//============================================================================
//      Type Definitions
//============================================================================
typedef signed char			int8_t;
typedef unsigned char		uint8_t;
typedef signed int			int16_t;
typedef unsigned int		uint16_t;
typedef signed long			int32_t;
typedef unsigned long		uint32_t;

//#define bool				unsigned char			// 1 Byte
#define BOOL  				unsigned char			// 1 Byte
#define CHAR				signed   char			// 1 Byte
#define UCHAR				unsigned char			// 1 Byte
#define uchar				unsigned char			// 1 Byte
#define BYTE				unsigned char			// 1 Byte
#define UINT				unsigned int			// 2 Bytes
#define USHORT				unsigned short			// 2 Bytes
#define ushort				unsigned short			// 2 Bytes
#define WORD				unsigned short			// 2 Bytes
#define DWORD				unsigned long			// 4 Bytes
#define SLONG				signed long			// 4 Bytes
#define ULONG				unsigned long			// 4 Bytes
#define LONG				unsigned long			// 4 Bytes

#define TRUE				0x01
#ifndef true
#define true				0x01
#endif
#define FALSE				0x00
#ifndef false
#define false				0x00
#endif

typedef union {
	BYTE b[2];
	UINT i;
}Byte2;

typedef union {
	BYTE b[4];
	ULONG l;
}Byte4;

//============================================================================
//      SFR Registers
//============================================================================
// ---------------------------------------------------------------------------
//  SFR_B(0x80, rP0); Expands to:
//
//  SFR(0x80, rP0);			// GPIO 0
//	SBIT(0x80, rP0_b0);		// GPIO 0.0
//	SBIT(0x81, rP0_b1);		// GPIO 0.1
//	SBIT(0x82, rP0_b2);		// GPIO 0.2
//	SBIT(0x83, rP0_b3);		// GPIO 0.3
//	SBIT(0x84, rP0_b4);		// GPIO 0.4
//	SBIT(0x85, rP0_b5);		// GPIO 0.5
//	SBIT(0x86, rP0_b6);		// GPIO 0.6
//	SBIT(0x87, rP0_b7);		// GPIO 0.7
// ---------------------------------------------------------------------------

/***************
*  SFR GROUP 80  *
****************/
SFR_B(0x80, rP0);		// General Purpose I/O 0
SFR(0x81, rSP);		// H/W STACK POINTER
SFR(0x82, rDP0L);		// DATA POINTER 0 - LOW  BYTE
SFR(0x83, rDP0H);		// DATA POINTER 0 - HIGH BYTE
SFR(0x84, rDP1L);		// DATA POINTER 1 - LOW  BYTE
SFR(0x85, rDP1H);		// DATA POINTER 1 - HIGH BYTE
SFR(0x86, rDPSEL);		// DPTR0/1 Selector register

SFR_B(0x88, rP5);		// General Purpose I/O 5
SFR(0x89, rKDTCON);		// Key Detector Control Register
SFR(0x8A, rKDTTIME);	// Key Detector Comparision Time Register
SFR(0x8B, rKDTRESULT);	// Key Detector Comparision Result Register
SFR(0x8C, rKDTCH);		// Key Detector Input Channel Select Register
SFR(0x8D, rKDTREF);		// Key Detector Reference Voltage Select Register

/***************
*  SFR GROUP 90  *
****************/
SFR_B(0x90, rP1);		// General Purpose I/O 1
SFR(0x93, rCLKCON);		// System Clock Control Register
SFR(0x94, rSMCLKCON);	// Smart Clock Control Register
SFR(0x95, rCLKEN0);		// Peripheral Clock Enable Register 0
SFR(0x96, rCLKEN1);		// Peripheral Clock Enable Register 1
SFR(0x97, rPCON);		// Power Control Register

SFR_B(0x98, rP6);		// General Purpose I/O 1
SFR(0x99, rRSTSTAT);	// Reset Source Status Register
SFR(0x9A, rSYSCFG);		// System configuration Register
SFR(0x9B, rLDOCON);		// LDO control Register 0
SFR(0x9C, rBLDCON);		// BLE control Register 1
SFR(0x9D, rWKUPSTAT);	// Wakeup Source Status Register
SFR(0x9F, rRSTCON);		// Reset Control Register

/***************
*  SFR GROUP A0  *
****************/

SFR_B(0xA0, rP2);		// General Purpose I/O 2
SFR(0xA1, rIE0);		// Interrupt Enable 0
SFR(0xA2, rIP0);		// Interrupt Priority 0
SFR(0xA3, rIE1);		// Interrupt Enable 1
SFR(0xA4, rIP1);		// Interrupt Priority 1
SFR(0xA5, rIE2);		// Interrupt Enable 2
SFR(0xA6, rIP2);		// Interrupt Priority 2
SFR(0xA8, rGIE);		// Global Interrupt Enable

/***************
*  SFR GROUP B0  *
****************/
SFR_B(0xB0, rP3);		// General Purpose I/O 3
SFR(0xB1, rCACON0);		// CAFG control Register 0
SFR(0xB2, rCACON1);		// CAFG control Register 1
SFR(0xB3, rCADATAL0);	// CAFG Leading Data Low Byte Register
SFR(0xB4, rCADATAL1);	// CAFG Leading Data High Byte Register
SFR(0xB5, rCADATAT0);	// CAFG Trailing Data Low Byte Register
SFR(0xB6, rCADATAT1);	// CAFG Trailing Data High Byte Register
SFR(0xB7, rCASTATUS);	// CAFG Status Register

SFR_B(0xB8, rP7);		// General Purpose I/O 7


/***************
*  SFR GROUP C0  *
****************/
SFR_B(0xC0, rP4);		// General Purpose I/O 4
SFR(0xC1, rT0CON);		// Timer 0 Control Register
SFR(0xC2, rT1CON);		// Timer 1 Control Register
SFR(0xC3, rT0DATA);		// Timer 0 Reference/Counting Data, Rising Captured Data Register
SFR(0xC4, rT1DATA);		// Timer 1 Reference/Counting Data, Rising Captured Data Register
SFR(0xC5, rT0PDR);		// Timer 0 PWM / Falling Captured Data Register
SFR(0xC6, rT1PDR);		// Timer 1 PWM / Falling Captured Data Register
SFR(0xC7, rT0STAT);		// Timer 0 Status Register
SFR(0xC8, rT1STAT);		// Timer 1 Status Register

SFR(0xC9, rRTCCON);		// RTC Control Register
SFR(0xCA, rRTCIEN);		// RTC Interrupt Enable Register
SFR(0xCB, rRTCIPND);	// RTC Interrupt Pending Register
SFR(0xCC, rRTCINTBS);	// RTC Interrupts Below Second Control Register
SFR(0xCD, rRTCSEC);		// RTC Second Register
SFR(0xCE, rRTCMIN);		// RTC Minute Register
SFR(0xCF, rRTCHOUR);	// RTC Hour Register


/***************
*  SFR GROUP D0  *
****************/
// 0xD0 PSW			// Program Status Word
SFR_B_BITS(0xD0, rPSW,		P, F1, OV, RS0, RS1, F0, AC, CY);
					// rPSW_P 		: Parity Flag
					// rPSW_F1 		: Flag1 available to the user for general purpose.
					// rPSW_OV 		: Overflow Flag
					// rPSW_RS0		: Register Bank Select 0
					// rPSW_RS1		: Register Bank Select 1
					// rPSW_F0 		: Flag0 available to the user for general purpose.
					// rPSW_AC 		: Auxiliary Carry Flag
					// rPSW_CY 		: Carry Flag

SFR(0xD1, rT2CON);		// Timer 2 Control Register
SFR(0xD2, rT3CON);		// Timer 3 Control Register
SFR(0xD3, rT2DATA);		// Timer 2 Reference/Counting/Rising Captured Data Register
SFR(0xD4, rT3DATA);		// Timer 3 Reference/Counting/Rising Captured Data Register
SFR(0xD5, rT2PDR);		// Timer 2 PWM / Falling Captured Data Register
SFR(0xD6, rT3PDR);		// Timer 3 PWM / Falling Captured Data Register
SFR(0xD7, rT2STAT);		// Timer 2 Status Register
SFR(0xD8, rT3STAT);		// Timer 3 Status Register


/***************
*  SFR GROUP E0  *
****************/
SFR(0xE0, rACC);		// ACC (Accumulator);

SFR(0xE9, rSCDCON);		// Sub Clock Divder Control Register
SFR(0xEA, rWTCON);		// Watch Timer Control Register
SFR(0xEB, rBUZCON);		// Buzzer Control Register
SFR(0xEC, rWDTCON);		// WDT Control Register
SFR(0xED, rWDTDIV);		// WDT Source Clock Division Ratio register
SFR(0xEE, rWDTCNT);		// WDT Counter Register
SFR(0xEF, rWDTREF);		// WDT Reference Value Register


/***************
*  SFR GROUP F0  *
****************/
SFR(0xF0, rB);			// B Register

SFR(0xF1, rU0CMD);		// UART 0 Command Register
SFR(0xF2, rU0CON0);		// UART 0 Control Register 0
SFR(0xF3, rU0CON1);		// UART 0 Control Register 1
SFR(0xF4, rU0RXSTAT);	// UART 0 RX Status Register
SFR(0xF5, rU0TXSTAT);	// UART 0 TX Status Register
SFR(0xF6, rU0EXSTAT);	// UART 0 Exception Status Register
SFR(0xF7, rU0IEN);		// UART 0 Interrupt Enable Register
SFR(0xF8, rU0IPND);		// UART 0 Interrupt Pending Register
SFR(0xF9, rU0CKDIVL);	// UART 0 Clock Divisor LSB Register
SFR(0xFA, rU0CKDIVM);	// UART 0 Clock Divisor MSB Register
SFR(0xFB, rU0BUF);		// UART 0 TX/RX Buffer

SFR(0xFC, rSPIMOD);		// SPI Mode Register
SFR(0xFD, rSPICK);		// SPI Baud Rate counter clock select Register
SFR(0xFE, rSPIDATA0);	// SPI Transmit and Receive Data 0
SFR(0xFF, rSPIDATA1);	// SPI Transmit and Receive Data 1



//============================================================================
//      Bit addressable SFR Registers
//============================================================================

// ---------------------------------------------------------------------------
//  SFR_B_BITS(0xC0, rTCON, T0RUN, T0CLR, T1RUN, T1CLR, T2RUN, T2CLR, RSVD6, RSVD7);
//
//  Expands to:
//
//  SFR(0xC0, rTCON);			// Timer 0/1/2 Control register
//	SBIT(0xC0, rTCON_T0RUN);	// Enable Timer 0
//	SBIT(0xC1, rTCON_T0CLR);	// Clear Timer 0 counter (T0CNT1, T0CNT0)
//	SBIT(0xC2, rTCON_T1RUN);	// Enable Timer 1
//	SBIT(0xC3, rTCON_T1CLR);	// Clear Timer 1 counter (T1CNT1, T1CNT0)
//	SBIT(0xC4, rTCON_T2RUN);	// Enable Timer 2
//	SBIT(0xC5, rTCON_T2CLR);	// Clear Timer 2 counter (T2CNT1, T2CNT0)
// ---------------------------------------------------------------------------


//============================================================================
//      Extended SFR Registers
//============================================================================

// PORT mode
// External Interrupt Controller & Pull-up Controller Register : 0xFE40
SFR16(0xFE40, rJTAGOFF);		// JTAG Port (GP30, 31, 32, 33) Control Register
SFR16(0xFE41, rEINTMOD0);	// External Interrupt Control Register 0
SFR16(0xFE42, rEINTMOD1);	// External Interrupt Control Register 1
SFR16(0xFE45, rEINTEN0);		// External Interrupt Enable Register 0
SFR16(0xFE47, rEINTPND0);	// External Interrupt Pending Register 0
SFR16(0xFE49, rP0PUR);		// PORT 0 Pull-Up Control Register
SFR16(0xFE4A, rP1PUR);		// PORT 1 Pull-Up Control Register
SFR16(0xFE4B, rP3PUR);		// PORT 3 Pull-Up Control Register
SFR16(0xFE4C, rP4PUR);		// PORT 4 Pull-Up Control Register
SFR16(0xFE4D, rP5PUR);		// PORT 5 Pull-Up Control Register
SFR16(0xFE4E, rP6PUR);		// PORT 5 Pull-Up Control Register
SFR16(0xFE4F, rP7PUR);		// PORT 5 Pull-Up Control Register

// General Purpose IO-0/1/2/3 Port Mode Register : 0xFE50
SFR16(0xFE50, rP0MOD0);		// GP00 ~ GP01 mode control register 0
SFR16(0xFE51, rP0MOD1);		// GP02 ~ GP03 mode control register 1
SFR16(0xFE52, rP0MOD2);		// GP04 ~ GP05 mode control register 2
SFR16(0xFE53, rP0MOD3);		// GP06 ~ GP07 mode control register 3
SFR16(0xFE54, rP1MOD0);		// GP10 ~ GP11 mode control register 0
SFR16(0xFE55, rP1MOD1);		// GP12 ~ GP13 mode control register 1
SFR16(0xFE56, rP1MOD2);		// GP14 ~ GP15 mode control register 2
SFR16(0xFE57, rP1MOD3);		// GP16 ~ GP17 mode control register 3
SFR16(0xFE58, rP3MOD0);		// GP30 ~ GP31 mode control register 0
SFR16(0xFE59, rP3MOD1);		// GP32 ~ GP33 mode control register 1
SFR16(0xFE5A, rP3MOD2);		// GP34 ~ GP35 mode control register 2
SFR16(0xFE5B, rP3MOD3);		// GP36 ~ GP37 mode control register 3
SFR16(0xFE5C, rP4MOD0);		// GP40 ~ GP41 mode control register 0
SFR16(0xFE5D, rP4MOD1);		// GP42 ~ GP43 mode control register 1
SFR16(0xFE5E, rP4MOD2);		// GP44 ~ GP45 mode control register 2
SFR16(0xFE5F, rP4MOD3);		// GP46 ~ GP47 mode control register 3

// General Purpose IO-5/6/7 Port Mode Register : 0xFE60
SFR16(0xFE60, rP5MOD0);		// GP50 ~ GP51 mode control register 0
SFR16(0xFE61, rP5MOD1);		// GP52 ~ GP53 mode control register 1
SFR16(0xFE62, rP5MOD2);		// GP54 ~ GP55 mode control register 2
SFR16(0xFE63, rP5MOD3);		// GP56 ~ GP57 mode control register 3
SFR16(0xFE64, rP6MOD0);		// GP60 ~ GP61 mode control register 0
SFR16(0xFE65, rP6MOD1);		// GP62 ~ GP63 mode control register 1
SFR16(0xFE66, rP6MOD2);		// GP64 ~ GP65 mode control register 2
SFR16(0xFE67, rP6MOD3);		// GP66 ~ GP67 mode control register 3
SFR16(0xFE68, rP7MOD0);		// GP70 ~ GP71 mode control register 0
SFR16(0xFE69, rP7MOD1);		// GP72 mode control register 1


// Flash Controller : 0xFFA0
SFR16(0xFFA0, rFLASHCTRL);	// Flash Control Register
SFR16(0xFFA1, rFLASHSTAT);	// Flash Status Register
SFR16(0xFFA2, rFLASHADDR0);	// Flash Address Low
SFR16(0xFFA3, rFLASHADDR1);	// Flash Address Middle
SFR16(0xFFA4, rFLASHADDR2);	// Flash Address High
SFR16(0xFFA6, rFLASHWDATA);	// Flash Write Data and Write Enable Register
SFR16(0xFFA8, rFLASHWP);		// Sector Program/Erase Protect Register
SFR16(0xFFAA, rXRAMADDR0);	// XRAM Base Low Register for DMA mode
SFR16(0xFFAB, rXRAMADDR1);	// XRAM Base High Register for DMA mode
SFR16(0xFFAC, rFLASHTC);		// Transferred byte count for DMA mode

// LCD: 0xFEC0
SFR16(0xFFC0, rLCON);		// LCD Control Register
SFR16(0xFFC1, rLMOD);		// LCD Mode Register
SFR16(0xFFC2, rLCKSEL);		// LCD Clock Select Register
SFR16(0xFFC3, rLCONTR);		// LCD Contrast Control Register
SFR16(0xFFC4, rLCONTRDYN);	// LCD Contrast Dynamic Control Register
SFR16(0xFFE0, rDISPMEM0);	// LCD Display Memory ...
SFR16(0xFFE1, rDISPMEM1);
SFR16(0xFFE2, rDISPMEM2);
SFR16(0xFFE3, rDISPMEM3);
SFR16(0xFFE4, rDISPMEM4);
SFR16(0xFFE5, rDISPMEM5);
SFR16(0xFFE6, rDISPMEM6);
SFR16(0xFFE7, rDISPMEM7);
SFR16(0xFFE8, rDISPMEM8);
SFR16(0xFFE9, rDISPMEM9);
SFR16(0xFFEA, rDISPMEM10);
SFR16(0xFFEB, rDISPMEM11);
SFR16(0xFFEC, rDISPMEM12);
SFR16(0xFFED, rDISPMEM13);
SFR16(0xFFEE, rDISPMEM14);
SFR16(0xFFEF, rDISPMEM15);

//============================================================================
//      Definitions for SFR Register
//============================================================================

// -> Register Name : "r"+"SFR Name"
// -> Bit Field Name : "SFR Name"+"_"+"Bit Name"
// -> Bit Field Value : "Bit Name"+"_"+"Value Mean"
// -> Direct Bit Field Value : "SFR Name"+"_"+"Bit Name"+"_"+"Value Mean"

// -----------------------------------------------------------------------
// Key Detector Controller
// -----------------------------------------------------------------------
// 0x89, rKDTCON
#define KDTCON_START			(0x80)
#define KDTCON_INTPND			(0x20)
#define KDTCON_MODE(x)			(x<<2)
#define KDTCON_INTEN			(0x02)
#define KDTCON_ENABLE			(0x01)

#define MODE_SINGLE				(0x0)
#define MODE_SAR				(0x1)
#define MODE_SLOPE				(0x2)

#define KDTCON_MODE_SINGLE		(KDTCON_MODE(MODE_SINGLE))
#define KDTCON_MODE_SAR			(KDTCON_MODE(MODE_SAR))
#define KDTCON_MODE_SLOPE		(KDTCON_MODE(MODE_SLOPE))

// 0x8A, rKDTTIME

// 0x8B, rKDTRESULT
#define KDTRESULT_MASK			(0x1F)

// 0x8C, rKDTCH
#define KDTCH_MASK				(0x03)

// 0x8D, rKDTREF
#define KDTREF_MASK				(0x1F)

// -----------------------------------------------------------------------
// System Controller
// -----------------------------------------------------------------------

// 0x93 rCLKCON 			// System Clock Control
#define CLKCON_SUBOP		(0x80)
#define CLKCON_CLKO(x)		(x<<4)
#define CLKCON_CLKDIV(x)		(x<<2)
#define CLKCON_CLKSRC(x)		(x<<0)

#define CLKO_RTC1			(0x7)
#define CLKO_RTC8			(0x6)
#define CLKO_SUB			(0x5)
#define CLKO_ESCLK			(0x4)
#define CLKO_EMCLK			(0x3)
#define CLKO_ISCLK			(0x2)
#define CLKO_IMCLK			(0x1)
#define CLKO_FSYS			(0x0)
#define CLKDIV_1			(0x3)
#define CLKDIV_2			(0x2)
#define CLKDIV_8			(0x1)
#define CLKDIV_16			(0x0)
#define CLKSRC_ESCLK		(0x3)
#define CLKSRC_EMCLK		(0x2)
#define CLKSRC_ISCLK		(0x1)
#define CLKSRC_IMCLK		(0x0)

#define CLKCON_CLKO_FSYS		(CLKCON_CLKO(CLKO_FSYS))
#define CLKCON_CLKO_IMCLK	(CLKCON_CLKO(CLKO_IMCLK))
#define CLKCON_CLKO_ISCLK	(CLKCON_CLKO(CLKO_ISCLK))
#define CLKCON_CLKO_EMCLK	(CLKCON_CLKO(CLKO_EMCLK))
#define CLKCON_CLKO_ESCLK	(CLKCON_CLKO(CLKO_ESCLK))
#define CLKCON_CLKO_SUB		(CLKCON_CLKO(CLKO_SUB))
#define CLKCON_CLKO_RTC8		(CLKCON_CLKO(CLKO_RTC8))
#define CLKCON_CLKO_RTC1		(CLKCON_CLKO(CLKO_RTC1))

#define CLKCON_CLKDIV_1		(CLKCON_CLKDIV(CLKDIV_1))
#define CLKCON_CLKDIV_2		(CLKCON_CLKDIV(CLKDIV_2))
#define CLKCON_CLKDIV_8		(CLKCON_CLKDIV(CLKDIV_8))
#define CLKCON_CLKDIV_16		(CLKCON_CLKDIV(CLKDIV_16))

#define CLKCON_CLKSRC_IMCLK	(CLKCON_CLKSRC(CLKSRC_IMCLK))
#define CLKCON_CLKSRC_ISCLK	(CLKCON_CLKSRC(CLKSRC_ISCLK))
#define CLKCON_CLKSRC_EMCLK	(CLKCON_CLKSRC(CLKSRC_EMCLK))
#define CLKCON_CLKSRC_ESCLK	(CLKCON_CLKSRC(CLKSRC_ESCLK))

// 0x94 rSMCLKCON 			// Smart Clock control
#define SMCLKCON_FLASHPDIDLE		(0x10)
#define SMCLKCON_FLASHWAITCNT(x)	(x<<2)

#define FLASHWAITCNT_1		(0x01)
#define FLASHWAITCNT_0		(0x00)

#define SMCLKCON_FLASHWAITCNT_1	(SMCLKCON_FLASHWAITCNT(FLASHWAITCNT_1))
#define SMCLKCON_FLASHWAITCNT_0	(SMCLKCON_FLASHWAITCNT(FLASHWAITCNT_0))

// 0x95 rCLKEN0 			// Peripheral Clock Enable Register 0
#define CLKEN0_UART0		(0x80)	// 7 : UART 0
#define CLKEN0_FLASH		(0x40)	// 6 : Flash
#define CLKEN0_TIMER23		(0x20)	// 5 : Timer 2/3
#define CLKEN0_TIMER01		(0x10)	// 4 : Timer 0/1
#define CLKEN0_SPI			(0x08)	// 3 : SPI
#define CLKEN0_LCD			(0x04)	// 2 : LCD
#define CLKEN0_WT			(0x02)	// 1 : Watch Timer
#define CLKEN0_WDT			(0x01)	// 0 : Watchdog timer

#define CLKEN0_ALL_EN		(rCLKEN0 = 0xFF)
#define CLKEN0_ALL_DIS		(rCLKEN0 = 0x00)

// 0x96 rCLKEN1 			// Peripheral Clock Enable Register 1
#define CLKEN1_KDT			(0x08)	// 3 : Key Detect
#define CLKEN1_RTC			(0x04)	// 2 : RTC
#define CLKEN1_SCD			(0x02)	// 1 : Clock Divider
#define CLKEN1_CAFG			(0x01)	// 0 : Carrier Frequency Generator

#define CLKEN1_ALL_EN		(rCLKEN1 = 0x0F)
#define CLKEN1_ALL_DIS		(rCLKEN1 = 0x00)

// 0x97 rPCON 				// Power Control Register
#define PCON_STOP			(0x02)
#define PCON_IDLE			(0x01)

// 0x99 rRSTSTAT 			// Reset Status Register
#define RSTSTAT_PORST		(0x10)
#define RSTSTAT_LVRST		(0x08)
#define RSTSTAT_WDTRST		(0x04)
#define RSTSTAT_SWRST		(0x02)
#define RSTSTAT_PINRST		(0x01)

// 0x9A, rSYSCFG			// System configuration register
#define SYSCFG_IMSTABLE		(0x80)
#define SYSCFG_EMSTABLE		(0x40)
#define SYSCFG_IMENSTOP		(0x20)
#define SYSCFG_EMENSTOP		(0x10)
#define SYSCFG_ISDIS		(0x08)
#define SYSCFG_IMDIS		(0x04)
#define SYSCFG_ESDIS		(0x02)
#define SYSCFG_EMDIS		(0x01)

// 0x9B, rLDOCON			// LDO Control Register
#define LDOCON_LPLDOSEL(x)	(x<<4)
#define LDOCON_LVRSEL(x)		(x<<1)
#define LDOCON_LVREN		(0x01)

#define LVRSEL_185V			(0x0)
#define LVRSEL_210V			(0x1)
#define LVRSEL_225V			(0x2)
#define LVRSEL_240V			(0x3)
#define LVRSEL_260V			(0x4)
#define LVRSEL_280V			(0x5)
#define LVRSEL_350V			(0x6)
#define LVRSEL_405V			(0x7)
#define LPLDOSEL_173V		(0xF)
#define LPLDOSEL_172V		(0xE)
#define LPLDOSEL_170V		(0xD)
#define LPLDOSEL_168V		(0xC)
#define LPLDOSEL_166V		(0xB)
#define LPLDOSEL_164V		(0xA)
#define LPLDOSEL_162V		(0x9)
#define LPLDOSEL_160V		(0x8)

#define LDOCON_LPLDOSEL_160V	(LDOCON_LPLDOSEL(LPLDOSEL_160V))
#define LDOCON_LPLDOSEL_162V	(LDOCON_LPLDOSEL(LPLDOSEL_162V))
#define LDOCON_LPLDOSEL_164V	(LDOCON_LPLDOSEL(LPLDOSEL_164V))
#define LDOCON_LPLDOSEL_166V	(LDOCON_LPLDOSEL(LPLDOSEL_166V))
#define LDOCON_LPLDOSEL_168V	(LDOCON_LPLDOSEL(LPLDOSEL_168V))
#define LDOCON_LPLDOSEL_170V	(LDOCON_LPLDOSEL(LPLDOSEL_170V))
#define LDOCON_LPLDOSEL_172V	(LDOCON_LPLDOSEL(LPLDOSEL_172V))
#define LDOCON_LPLDOSEL_173V	(LDOCON_LPLDOSEL(LPLDOSEL_173V))

#define LDOCON_LVRSEL_185V	(LDOCON_LVRSEL(LVRSEL_185V))
#define LDOCON_LVRSEL_210V	(LDOCON_LVRSEL(LVRSEL_210V))
#define LDOCON_LVRSEL_225V	(LDOCON_LVRSEL(LVRSEL_225V))
#define LDOCON_LVRSEL_240V	(LDOCON_LVRSEL(LVRSEL_240V))
#define LDOCON_LVRSEL_260V	(LDOCON_LVRSEL(LVRSEL_260V))
#define LDOCON_LVRSEL_280V	(LDOCON_LVRSEL(LVRSEL_280V))
#define LDOCON_LVRSEL_350V	(LDOCON_LVRSEL(LVRSEL_350V))
#define LDOCON_LVRSEL_405V	(LDOCON_LVRSEL(LVRSEL_405V))

// 0x9C, rBLDCON			// BLD Control Register
#define BLDCON_BLDOUT		(0x10)
#define BLDCON_BLDSEL(x)		(x<<1)
#define BLDCON_BLDEN		(0x01)

#define BLDSEL_185V		(0x0)
#define BLDSEL_210V		(0x1)
#define BLDSEL_225V		(0x2)
#define BLDSEL_240V		(0x3)
#define BLDSEL_260V		(0x4)
#define BLDSEL_280V		(0x5)
#define BLDSEL_350V		(0x6)
#define BLDSEL_120V		(0x7)

#define BLDCON_BLDSEL_185V	(BLDCON_BLDSEL(BLDSEL_185V))
#define BLDCON_BLDSEL_210V	(BLDCON_BLDSEL(BLDSEL_210V))
#define BLDCON_BLDSEL_225V	(BLDCON_BLDSEL(BLDSEL_225V))
#define BLDCON_BLDSEL_240V	(BLDCON_BLDSEL(BLDSEL_240V))
#define BLDCON_BLDSEL_260V	(BLDCON_BLDSEL(BLDSEL_260V))
#define BLDCON_BLDSEL_280V	(BLDCON_BLDSEL(BLDSEL_280V))
#define BLDCON_BLDSEL_350V	(BLDCON_BLDSEL(BLDSEL_350V))
#define BLDCON_BLDSEL_120V	(BLDCON_BLDSEL(BLDSEL_120V))

// 0x9D, rWKUPSTAT			// WakeUp State Register from STOP
#define WKUPSTAT_EINTWKUP	(0x08)	// Wake-up by Watch timer from STOP mode
#define WKUPSTAT_RTCWKUP		(0x10)	// Wake-up by Watchdog timer from STOP mode
#define WKUPSTAT_WDTWKUP		(0x20)	// Wake-up by RTC Interrupt from STOP mode
#define WKUPSTAT_WTWKUP		(0x40)	// Wake-up by External Interrupt from STOP mode

// 0x9F rRSTCON 			// Reset Control Register
#define RSTCON_SWRST		(0x0B)


// -----------------------------------------------------------------------
// Interrupt Controller
// -----------------------------------------------------------------------

// 0xA1 rIE0 				// Interrupt Enable-0 Register Bit's define.
#define IE0_TIMER0			(0x80)
#define IE0_SPI			(0x40)
#define IE0_EINT1			(0x20)
#define IE0_EINT0			(0x10)
#define IE0_WT				(0x08)
#define IE0_RTC			(0x04)
#define IE0_UART			(0x02)
#define IE0_WDT			(0x01)

#define IE0_ALL_EN			(0xFF)
#define IE0_ALL_DIS			(0x00)

// 0xA2 rIP0 				// Interrupt Priority-0 Register bit's define.
#define IP0_TIMER0			(0x80)
#define IP0_SPI			(0x40)
#define IP0_EINT1			(0x20)
#define IP0_EINT0			(0x10)
#define IP0_WT				(0x08)
#define IP0_RTC			(0x04)
#define IP0_UART			(0x02)
#define IP0_WDT			(0x01)

// 0xA3 rIE1 				// Interrupt Enable-1 Register bit's define.
#define IE1_EINT4			(0x80)
#define IE1_EINT3			(0x40)
#define IE1_EINT2			(0x20)
#define IE1_TIMER3			(0x10)
#define IE1_CAFG			(0x08)
#define IE1_TIMER1			(0x04)
#define IE1_KDT			(0x02)
#define IE1_TIMER2			(0x01)

#define IE1_ALL_EN			(0xFF)
#define IE1_ALL_DIS			(0x00)

// 0xA4 rIP1				// Interrupt Priority-1 register bit's define.
#define IP1_EINT4			(0x80)
#define IP1_EINT3			(0x40)
#define IP1_EINT2			(0x20)
#define IP1_TIMER3			(0x10)
#define IP1_CAFG			(0x08)
#define IP1_TIMER1			(0x04)
#define IP1_KDT			(0x02)
#define IP1_TIMER2			(0x01)

// 0xA5 rIE2 				// Interrupt Enable-2 Register bit's define.
#define IE2_EINT7			(0x04)
#define IE2_EINT6			(0x02)
#define IE2_EINT5			(0x01)

#define IE2_ALL_EN			(0x07)
#define IE2_ALL_DIS			(0x00)

// 0xA6 rIP2 				// Interrupt Priority-2 Register bit's define.
#define IP2_EINT7			(0x04)
#define IP2_EINT6			(0x02)
#define IP2_EINT5			(0x01)

// 0xA8 rGIE				// INT Global Interrupt Enable
#define GIE_ENABLE			(0x80)

//#define rGIE_GIE(x)	((x == 1) ? (rGIE |= (1 << 7)) : (x == 0) ? (rGIE &= ~(1 << 7)) : (rGIE & 0x80))
#define EnterCriticalSection    rGIE = 0x00
#define ExitCriticalSection     rGIE = GIE_ENABLE
#define GlobalIntDisable        rGIE = 0x00
#define GlobalIntEnable         rGIE = GIE_ENABLE


// -----------------------------------------------------------------------
// SPI
// -----------------------------------------------------------------------
// 0xFC rSPIMOD				// SPI Mode Register
#define SPIMOD_CSNCON		(0x80)
#define SPIMOD_CSNPOL		(0x40)
#define SPIMOD_CKPHA		(0x20)
#define SPIMOD_CKPOL		(0x10)
#define SPIMOD_SLVEN		(0x08)
#define SPIMOD_LSBF			(0x04)
#define SPIMOD_MODE			(0x02)
#define SPIMOD_ENABLE		(0x01)

#define SPIMOD_CSNCON_HW		(0x00)
#define SPIMOD_CSNCON_SW		(0x80)
#define SPIMOD_CSNPOL_LOW	(0x00)
#define SPIMOD_CSNPOL_HIGH	(0x40)
#define SPIMOD_CKPHA_FIRST	(0x00)
#define SPIMOD_CKPHA_SECOND	(0x20)
#define SPIMOD_CKPOL_LOW		(0x00)
#define SPIMOD_CKPOL_HIGH	(0x10)
#define SPIMOD_SLVEN_MASTER	(0x00)
#define SPIMOD_SLVEN_SLAVE	(0x08)
#define SPIMOD_LSBF_MSB		(0x00)
#define SPIMOD_LSBF_LSB		(0x04)
#define SPIMOD_MODE_RX		(0x00)
#define SPIMOD_MODE_RXTX		(0x02)

// 0xFD rSPICK				// SPI Baud Rate counter clock select Register
#define SPICK_DATA16		(0x40)
#define SPICK_INTPEND		(0x20)
#define SPICK_DIVIDER(n)		(n<<0)

#define DIVIDER_4095			(0x0)
#define DIVIDER_2500			(0x1)
#define DIVIDER_1250			(0x2)
#define DIVIDER_1000			(0x3)
#define DIVIDER_500			(0x4)
#define DIVIDER_250			(0x5)
#define DIVIDER_125			(0x6)
#define DIVIDER_50			(0x7)
#define DIVIDER_25			(0x8)
#define DIVIDER_12			(0x9)
#define DIVIDER_10			(0xA)
#define DIVIDER_5			(0xB)
#define DIVIDER_4			(0xC)

#define SPICK_DIVIDER_4095	(SPICK_DIVIDER(DIVIDER_4095))
#define SPICK_DIVIDER_2500	(SPICK_DIVIDER(DIVIDER_2500))
#define SPICK_DIVIDER_1250	(SPICK_DIVIDER(DIVIDER_1250))
#define SPICK_DIVIDER_1000	(SPICK_DIVIDER(DIVIDER_1000))
#define SPICK_DIVIDER_500	(SPICK_DIVIDER(DIVIDER_500))
#define SPICK_DIVIDER_250	(SPICK_DIVIDER(DIVIDER_250))
#define SPICK_DIVIDER_125	(SPICK_DIVIDER(DIVIDER_125))
#define SPICK_DIVIDER_50		(SPICK_DIVIDER(DIVIDER_50))
#define SPICK_DIVIDER_25		(SPICK_DIVIDER(DIVIDER_25))
#define SPICK_DIVIDER_12		(SPICK_DIVIDER(DIVIDER_12))
#define SPICK_DIVIDER_10		(SPICK_DIVIDER(DIVIDER_10))
#define SPICK_DIVIDER_5		(SPICK_DIVIDER(DIVIDER_5))
#define SPICK_DIVIDER_4		(SPICK_DIVIDER(DIVIDER_4))

// 0xFE rSPIDATA0			// SPI Transmit and Receive Data-0 Register.
// 0xFF rSPIDATA1			// SPI Transmit and Receive Data-1 Register.


// -----------------------------------------------------------------------
// UART
// -----------------------------------------------------------------------
// 0xF1 rU0CMD				// UART Command Register
#define U0CMD_RFEN			(0x40)
#define U0CMD_TFEN			(0x20)
#define U0CMD_RFFCLR		(0x10)
#define U0CMD_TFFCLR		(0x08)
#define U0CMD_SBR			(0x04)
#define U0CMD_RXEN			(0x02)
#define U0CMD_TXEN			(0x01)

// 0xF2 rU0CON0			// UART Control Register 0
#define U0CON0_PMD(x)		(x<<5)
#define U0CON0_LOOPB		(0x10)
#define U0CON0_ECHO			(0x08)
#define U0CON0_STB			(0x04)
#define U0CON0_WL(x)		(x<<0)

#define WL_5				(0x0)
#define WL_6				(0x1)
#define WL_7				(0x2)
#define WL_8				(0x3)
#define PMD_NO				(0x0)
#define PMD_ODD			(0x4)
#define PMD_EVEN			(0x5)
#define PMD_FORCE1			(0x6)
#define PMD_FORCE0			(0x7)

#define U0CON0_PMD_NO		(U0CON0_PMD(PMD_NO))
#define U0CON0_PMD_ODD		(U0CON0_PMD(PMD_ODD))
#define U0CON0_PMD_EVEN		(U0CON0_PMD(PMD_EVEN))
#define U0CON0_PMD_FORCE1	(U0CON0_PMD(PMD_FORCE1))
#define U0CON0_PMD_FORCE0	(U0CON0_PMD(PMD_FORCE0))

#define U0CON0_STB_1BIT		(0x00)
#define U0CON0_STB_2BIT		(0x04)

#define U0CON0_WL_5BIT		(U0CON0_WL(WL_5))
#define U0CON0_WL_6BIT		(U0CON0_WL(WL_6))
#define U0CON0_WL_7BIT		(U0CON0_WL(WL_7))
#define U0CON0_WL_8BIT		(U0CON0_WL(WL_8))

// 0xF3 rU0CON1				// UART Control Register 1
#define U0CON1_RTOV(x)		(x<<4)
#define U0CON1_CDIBW(x)		(x<<0)

// 0xF4 rU0RXSTAT			// UART RX Status Register
#define U0RXSTAT_RXI		(0x40)
#define U0RXSTAT_RDC(x)		(x<<1)
#define U0RXSTAT_RDV		(0x01)
#define U0RXSTAT_RDC_MASK	(0x3E)
#define RDC_0				(0x0)
#define RDC_1				(0x1)
#define RDC_2				(0x2)

// 0xF5 rU0TXSTAT			// UART TX Status Register
#define U0TXSTAT_TDC(x)		(x<<1)
#define U0TXSTAT_TXI		(0x01)
#define U0TXSTAT_TDC_MASK	(0x3E)
#define TDC_0				(0x0)
#define TDC_1				(0x1)
#define TDC_2				(0x2)

// 0xF6 rU0EXSTAT			// UART Error Status Register
#define U0EXSTAT_RTOUT		(0x20)
#define U0EXSTAT_UNDR		(0x10)
#define U0EXSTAT_OVF		(0x08)
#define U0EXSTAT_PER		(0x04)
#define U0EXSTAT_FER		(0x02)
#define U0EXSTAT_BKD		(0x01)
#define U0EXSTAT_ALL		(0x3F)

// 0xF7 rU0IEN				// UART Interrupt Enable Register
#define U0IEN_ERROR			(0x80)
#define U0IEN_TXEOF			(0x40)
#define U0IEN_HTE			(0x20)
#define U0IEN_TFE			(0x10)
#define U0IEN_TXI			(0x08)
#define U0IEN_HRV			(0x04)
#define U0IEN_RFF			(0x02)
#define U0IEN_RXEOF			(0x01)
#define U0IEN_ALL			(0xFF)

// 0xF8 rU0IPND				// UART Interrupt Pending Register
#define U0IPND_ERROR		(0x80)
#define U0IPND_TXEOF		(0x40)
#define U0IPND_HTE			(0x20)
#define U0IPND_TFE			(0x10)
#define U0IPND_TXI			(0x08)
#define U0IPND_HRV			(0x04)
#define U0IPND_RFF			(0x02)
#define U0IPND_RXEOF		(0x01)
#define U0IPND_ALL			(0xFF)

// 0xF9 rU0CKDVM			// UART Clock divider MSB Register
// 0xFA rU0CKDVL			// UART Clock divider LSB Register
// 0xFB rU0BUF				// UART TX/RX Buffer


// -----------------------------------------------------------------------
// Timers
// -----------------------------------------------------------------------

// 0xC1 rT0CON				// TIMER 0 Control Register
// 0xC2 rT1CON				// TIMER 1 Control Register
// 0xD1 rT2CON				// TIMER 2 Control Register
// 0xD2 rT3CON				// TIMER 3 Control Register
#define TnCON_CLKSEL(x)		(x<<4)
#define TnCON_MODSEL(x)		(x<<2)
#define TnCON_RUN(x)		(x<<0)

#define MODSEL_INTERVAL		(0x0)
#define MODSEL_PWM			(0x1)
#define MODSEL_CAPTURE		(0x2)
#define RUN_STOP			(0x0)
#define RUN_RESUME			(0x2)
#define RUN_START			(0x3)

#define TnCON_MODSEL_INTERVAL	(TnCON_MODSEL(MODSEL_INTERVAL))
#define TnCON_MODSEL_PWM		(TnCON_MODSEL(MODSEL_PWM))
#define TnCON_MODSEL_CAPTURE	(TnCON_MODSEL(MODSEL_CAPTURE))

#define TnCON_RUN_STOP		(TnCON_RUN(RUN_STOP))
#define TnCON_RUN_RESUME		(TnCON_RUN(RUN_RESUME))
#define TnCON_RUN_START		(TnCON_RUN(RUN_START))

// 0xC3 T0DATA
// 0xC4 T1DATA
// 0xC5 T0PDR
// 0xC6 T1PDR

// 0xC7 rT0STAT				// Timer 0 Status Register
// 0xC8 rT1STAT				// Timer 1 Status Register
// 0xD7 rT2STAT				// Timer 2 Status Register
// 0xD8 rT3STAT				// Timer 3 Status Register
#define TnSTAT_CAPF			(0x80)
#define TnSTAT_CAPR			(0x40)
#define TnSTAT_MATCH		(0x20)
#define TnSTAT_INTPND		(0x10)
#define TnSTAT_16BIT		(0x08)
#define TnSTAT_CNTTYPE		(0x04)
#define TnSTAT_STOPTYPE		(0x02)
#define TnSTAT_TOUT			(0x01)

#define TnSTAT_STOPTYPE_PERIOD	(0x00)
#define TnSTAT_STOPTYPE_IMMEDIATE	(0x02)

#define TnSTAT_TOUT_LOW		(0x00)
#define TnSTAT_TOUT_HIGH		(0x01)

// 0xD3 T2DATA
// 0xD4 T3DATA
// 0xD5 T2PDR
// 0xD6 T3PDR

// -----------------------------------------------------------------------
// CAFG (Carrier Frequency Generator)
// -----------------------------------------------------------------------
// 0xB1 rCACON0				// CAFG Control Register 0
#define CACON0_INTPND		(0x80)	// Interrupt Pending Bit
#define CACON0_INTTYPE(x)	(x<<5)	// Elapsed time for low data value
#define CACON0_INTEN		(0x10)	// Interrupt enable bit
#define CACON0_STOP			(0x08)	// Stop bit
#define CACON0_START		(0x04)	// Start bit
#define CACON0_RPT			(0x02)	// mode selection bit
#define CACON0_OSP			(0x01)	// Output signal starting polarity control

#define INTTYPE_LEAD		(0x0)
#define INTTYPE_TRAIL		(0x1)
#define INTTYPE_BOTH		(0x2)
#define INTTYPE_NO			(0x3)

#define CACON0_INTTYPE_LEAD	(CACON0_INTTYPE(INTTYPE_LEAD))
#define CACON0_INTTYPE_TRAIL	(CACON0_INTTYPE(INTTYPE_TRAIL))
#define CACON0_INTTYPE_BOTH	(CACON0_INTTYPE(INTTYPE_BOTH))
#define CACON0_INTTYPE_NO	(CACON0_INTTYPE(INTTYPE_NO))

#define CACON0_OSP_LOW		(0x00)
#define CACON0_OSP_HIGH		(0x01)

// 0xB2 rCACON1				// CAFG Control Register 1
#define CACON1_CLKSEL(x)		(x<<6)
#define CACON1_CARRIER		(0x20)
#define CACON1_ENVELOP		(0x10)
#define CACON1_REMSTAT		(0x08)
#define CACON1_TMATCH		(0x04)
#define CACON1_HWRELD		(0x02)
#define CACON1_FRCLD		(0x01)

#define CLKSEL_DIV1			(0x0)
#define CLKSEL_DIV2			(0x1)
#define CLKSEL_DIV4			(0x2)
#define CLKSEL_DIV8			(0x3)

#define CACON1_CLKSEL_DIV1	(CACON1_CLKSEL(CLKSEL_DIV1))
#define CACON1_CLKSEL_DIV2	(CACON1_CLKSEL(CLKSEL_DIV2))
#define CACON1_CLKSEL_DIV4	(CACON1_CLKSEL(CLKSEL_DIV4))
#define CACON1_CLKSEL_DIV8	(CACON1_CLKSEL(CLKSEL_DIV8))

#define CACON1_REMSTAT_LOW	(0x00)
#define CACON1_REMSTAT_HIGH	(0x08)

// 0xB3 rCADATAL0			// CAFG Leading Data Low Register
// 0xB4 rCADATAL1			// CAFG Leading Data High Register
// 0xB5 rCADATAT0			// CAFG Trailing Data Low Register
// 0xB6 rCADATAT1			// CAFG Trailing Data High Register

// 0xB7 rCASTATUS			// CAFG Status Register
#define CASTATUS_STATUS(x)	(x<<0)

#define STATUS_LEAD			(0x1)
#define STATUS_TRAIL		(0x2)

#define CASTATUS_STATUS_LEAD	(0x01)
#define CASTATUS_STATUS_TRAIL	(0x02)

// -----------------------------------------------------------------------
// WDT
// -----------------------------------------------------------------------
// 0xEC rWDTCON				// WDT Control Register
#define WDTCON_CLKSEL(x)		(x<<6)
#define WDTCON_INTPEND		(0x20)
#define WDTCON_CNTCLR		(0x10)
#define WDTCON_WAKEEN		(0x08)
#define WDTCON_FUNCSEL(x)	(x<<1)
#define WDTCON_ENABLE		(0x01)

#define CLKSEL_IMCLK		(0x0)
#define CLKSEL_ISCLK		(0x1)
#define CLKSEL_EMCLK		(0x2)
#define CLKSEL_ESCLK		(0x3)
#define FUNCSEL_ALLDISABLE	(0x0)
#define FUNCSEL_RSTENABLE	(0x1)
#define FUNCSEL_INTENABLE	(0x2)
#define FUNCSEL_ALLENABLE	(0x3)

#define WDTCON_CLKSEL_IMCLK	(WDTCON_CLKSEL(CLKSEL_IMCLK))
#define WDTCON_CLKSEL_ISCLK	(WDTCON_CLKSEL(CLKSEL_ISCLK))
#define WDTCON_CLKSEL_EMCLK	(WDTCON_CLKSEL(CLKSEL_EMCLK))
#define WDTCON_CLKSEL_ESCLK	(WDTCON_CLKSEL(CLKSEL_ESCLK))

#define WDTCON_FUNCSEL_INTD_RSTD	(WDTCON_FUNCSEL(FUNCSEL_ALLDISABLE))
#define WDTCON_FUNCSEL_INTD_RSTE	(WDTCON_FUNCSEL(FUNCSEL_RSTENABLE))
#define WDTCON_FUNCSEL_INTE_RSTD	(WDTCON_FUNCSEL(FUNCSEL_INTENABLE))
#define WDTCON_FUNCSEL_INTE_RSTE	(WDTCON_FUNCSEL(FUNCSEL_ALLENABLE))

#define WDT_DISABLE			(rWDTCON &= ~WDTCON_ENABLE)
#define WDT_ENABLE			(rWDTCON |= WDTCON_ENABLE)

// 0xED rWDTDIV			// WDT Counter Pre-scale value Register
#define WDTDIV_MASK			(0x0F)
#define WDTDIV_DIV(x)		(x<<0)

// 0xEE rWDTCNT			// WDT Counter Register
// 0xEF rWDTREF			// WDT Reference value Register


// -----------------------------------------------------------------------
// WATCH TIMER
// -----------------------------------------------------------------------
// 0xEA rWTCON				// Watch Timer Control Register
#define WTCON_WAKEEN		(0x80)
#define WTCON_CLKSEL(x)		(x<<5)
#define WTCON_INTEN			(0x10)
#define WTCON_COUNT(x)		(x<<2)
#define WTCON_WTEN			(0x02)
#define WTCON_INTPND		(0x01)

#define CLKSEL_IM256		(0x0)
#define CLKSEL_IS			(0x1)
#define CLKSEL_EM256		(0x2)
#define CLKSEL_ES			(0x3)
#define COUNT_128			(0x0)
#define COUNT_8192		(0x1)
#define COUNT_16384		(0x2)
#define COUNT_32768		(0x3)

#define WTCON_CLKSEL_IMCLK256	(WTCON_CLKSEL(CLKSEL_IM256))
#define WTCON_CLKSEL_ISCLK	(WTCON_CLKSEL(CLKSEL_IS))
#define WTCON_CLKSEL_EMCLK256	(WTCON_CLKSEL(CLKSEL_EM256))
#define WTCON_CLKSEL_ESCLK	(WTCON_CLKSEL(CLKSEL_ES))

#define WTCON_COUNT_128		(WTCON_COUNT(COUNT_128))
#define WTCON_COUNT_8192		(WTCON_COUNT(COUNT_8192))
#define WTCON_COUNT_16384	(WTCON_COUNT(COUNT_16384))
#define WTCON_COUNT_32768	(WTCON_COUNT(COUNT_32768))

// -----------------------------------------------------------------------
// BUZZER
// -----------------------------------------------------------------------
// 0xEB rBUZCON			// BUZZER Control Register
#define BUZCON_BUZZER(x)		(x<<4)
#define BUZCON_BUZPOL		(0x02)
#define BUZCON_ENABLE		(0x01)

#define BUZPOL_LOW			(0x1)
#define BUZPOL_HIGH			(0x0)

#define BUZZER_1HZ			(0x0)
#define BUZZER_2HZ			(0x1)
#define BUZZER_4HZ			(0x2)
#define BUZZER_8HZ			(0x3)
#define BUZZER_16HZ			(0x4)
#define BUZZER_32HZ			(0x5)
#define BUZZER_64HZ			(0x6)
#define BUZZER_128HZ		(0x7)
#define BUZZER_256HZ		(0x8)
#define BUZZER_512HZ		(0x9)
#define BUZZER_1024HZ		(0xA)
#define BUZZER_2048HZ		(0xB)
#define BUZZER_4096HZ		(0xC)
#define BUZZER_8192HZ		(0xD)
#define BUZZER_16384HZ		(0xE)

#define BUZCON_BUZZER_1HZ	(BUZCON_BUZZER(BUZZER_1HZ))
#define BUZCON_BUZZER_2HZ	(BUZCON_BUZZER(BUZZER_2HZ))
#define BUZCON_BUZZER_4HZ	(BUZCON_BUZZER(BUZZER_4HZ))
#define BUZCON_BUZZER_8HZ	(BUZCON_BUZZER(BUZZER_8HZ))
#define BUZCON_BUZZER_16HZ	(BUZCON_BUZZER(BUZZER_16HZ))
#define BUZCON_BUZZER_32HZ	(BUZCON_BUZZER(BUZZER_32HZ))
#define BUZCON_BUZZER_64HZ	(BUZCON_BUZZER(BUZZER_64HZ))
#define BUZCON_BUZZER_128HZ	(BUZCON_BUZZER(BUZZER_128HZ))
#define BUZCON_BUZZER_256HZ	(BUZCON_BUZZER(BUZZER_256HZ))
#define BUZCON_BUZZER_512HZ	(BUZCON_BUZZER(BUZZER_512HZ))
#define BUZCON_BUZZER_1024HZ	(BUZCON_BUZZER(BUZZER_1024HZ))
#define BUZCON_BUZZER_2048HZ	(BUZCON_BUZZER(BUZZER_2048HZ))
#define BUZCON_BUZZER_4096HZ	(BUZCON_BUZZER(BUZZER_4096HZ))
#define BUZCON_BUZZER_8192HZ	(BUZCON_BUZZER(BUZZER_8192HZ))
#define BUZCON_BUZZER_16384HZ	(BUZCON_BUZZER(BUZZER_16384HZ))

#define BUZCON_BUZPOL_LOW	(0x02)
#define BUZCON_BUZPOL_HIGH	(0x00)


// -----------------------------------------------------------------------
// Sub-Clock Divider Controller
// -----------------------------------------------------------------------
// 0xE9 rSCDCON				// Clock Divider Control Register
#define SCDCON_DIVEN		(0x80)
#define SCDCON_CLKSEL		(0x40)
#define SCDCON_TRIM			(0x20)
#define SCDCON_TRIMVAL(x)	(x<<0)

#define SCDCON_CLKSEL_ESCLK	(0x00)
#define SCDCON_CLKSEL_ISCLK	(0x40)

// -----------------------------------------------------------------------
// RTC
// -----------------------------------------------------------------------

// 0xC9 rRTCCON				// RTC Control Register
#define RTCCON_WAKEEN		(0x80)
#define RTCCON_PTRIM(x)		(x<<4)
#define RTCCON_UPDPND		(0x08)
#define RTCCON_BCD			(0x04)
#define RTCCON_MODE24		(0x02)
#define RTCCON_RUN			(0x01)

#define PTRIM_NOT			(0x0)
#define PTRIM_1SEC			(0x1)
#define PTRIM_10SEC			(0x2)
#define PTRIM_1MIN			(0x3)
#define PTRIM_10MIN			(0x4)
#define PTRIM_1HOUR			(0x5)
#define PTRIM_12HOUR		(0x6)
#define PTRIM_24HOUR		(0x7)

#define RTCCON_PTRIM_NOT		(RTCCON_PTRIM(PTRIM_NOT))
#define RTCCON_PTRIM_1SEC	(RTCCON_PTRIM(PTRIM_1SEC))
#define RTCCON_PTRIM_10SEC	(RTCCON_PTRIM(PTRIM_10SEC))
#define RTCCON_PTRIM_1MIN	(RTCCON_PTRIM(PTRIM_1MIN))
#define RTCCON_PTRIM_10MIN	(RTCCON_PTRIM(PTRIM_10MIN))
#define RTCCON_PTRIM_1HOUR	(RTCCON_PTRIM(PTRIM_1HOUR))
#define RTCCON_PTRIM_12HOUR	(RTCCON_PTRIM(PTRIM_12HOUR))
#define RTCCON_PTRIM_24HOUR	(RTCCON_PTRIM(PTRIM_24HOUR))


// 0xCA rRTCIEN				// RTC Interrupt Enable Register
#define RTCIEN_24HR			(0x40)
#define RTCIEN_12HR			(0x20)
#define RTCIEN_1HR			(0x10)
#define RTCIEN_10MIN		(0x08)
#define RTCIEN_1MIN			(0x04)
#define RTCIEN_10SEC		(0x02)
#define RTCIEN_1SEC			(0x01)

// 0xCB rRTCIPND			// RTC Interrupt Enable Register
#define RTCIPND_24HR		(0x40)
#define RTCIPND_12HR		(0x20)
#define RTCIPND_1HR			(0x10)
#define RTCIPND_10MIN		(0x08)
#define RTCIPND_1MIN		(0x04)
#define RTCIPND_10SEC		(0x02)
#define RTCIPND_1SEC		(0x01)

// 0xCC rRTCINTBS			// RTC Interrupt Below Second Control Register
#define RTCINTBS_2HZEN			(0x80)
#define RTCINTBS_4HZEN			(0x40)
#define RTCINTBS_8HZEN			(0x20)
#define RTCINTBS_256HZEN			(0x10)
#define RTCINTBS_2HZPND			(0x08)
#define RTCINTBS_4HZPND			(0x04)
#define RTCINTBS_8HZPND			(0x02)
#define RTCINTBS_256HZPND		(0x01)

// 0xCD rRTCSEC			// RTC Second Register
#define SEC_MASK			(0x7F)

// 0xCE rRTCMIN			// RTC Minute Register
#define MIN_MASK			(0x7F)

// 0xCF rRTCHOUR			// RTC Hour Register
#define RTCHOUR_AMPM		(0x80)
#define HOUR_MASK			(0x3F)



// -----------------------------------------------------------------------
// GPIO mode
// -----------------------------------------------------------------------
// 0x80 rP0 				// 
// 0x90 rP1 				// 
// 0xB0 rP3 				// 
// 0xC0 rP4 				// 
// 0x88 rP5 				// 
// 0x98 rP6 				// 
// 0xB8 rP7 				// 
// 0xFE40 rJTAGOFF 			// JTAG port disable control Register
#define JTAGOFF_DISABLE		(0x01)

// 0xFE41 rEINTMOD0 		// External Interrupt Control Register 0
#define EINTMOD0_EINT3CON(x)	(x<<6)
#define EINTMOD0_EINT2CON(x)	(x<<4)
#define EINTMOD0_EINT1CON(x)	(x<<2)
#define EINTMOD0_EINT0CON(x)	(x<<0)

#define EINT3CON_RISE		(0x1)
#define EINT3CON_FALL		(0x2)
#define EINT3CON_BOTH		(0x3)
#define EINT2CON_RISE		(0x1)
#define EINT2CON_FALL		(0x2)
#define EINT2CON_BOTH		(0x3)
#define EINT1CON_RISE		(0x1)
#define EINT1CON_FALL		(0x2)
#define EINT1CON_BOTH		(0x3)
#define EINT0CON_RISE		(0x1)
#define EINT0CON_FALL		(0x2)
#define EINT0CON_BOTH		(0x3)

#define EINTMOD0_EINT0CON_RISE	(EINTMOD0_EINT0CON(EINT0CON_RISE))
#define EINTMOD0_EINT0CON_FALL	(EINTMOD0_EINT0CON(EINT0CON_FALL))
#define EINTMOD0_EINT0CON_BOTH	(EINTMOD0_EINT0CON(EINT0CON_BOTH))
#define EINTMOD0_EINT1CON_RISE	(EINTMOD0_EINT1CON(EINT1CON_RISE))
#define EINTMOD0_EINT1CON_FALL	(EINTMOD0_EINT1CON(EINT1CON_FALL))
#define EINTMOD0_EINT1CON_BOTH	(EINTMOD0_EINT1CON(EINT1CON_BOTH))
#define EINTMOD0_EINT2CON_RISE	(EINTMOD0_EINT2CON(EINT2CON_RISE))
#define EINTMOD0_EINT2CON_FALL	(EINTMOD0_EINT2CON(EINT2CON_FALL))
#define EINTMOD0_EINT2CON_BOTH	(EINTMOD0_EINT2CON(EINT2CON_BOTH))
#define EINTMOD0_EINT3CON_RISE	(EINTMOD0_EINT3CON(EINT3CON_RISE))
#define EINTMOD0_EINT3CON_FALL	(EINTMOD0_EINT3CON(EINT3CON_FALL))
#define EINTMOD0_EINT3CON_BOTH	(EINTMOD0_EINT3CON(EINT3CON_BOTH))

// 0xFE42 rEINTMOD1 		// External Interrupt Control Register 1
#define EINTMOD1_EINT7CON(x)	(x<<6)
#define EINTMOD1_EINT6CON(x)	(x<<4)
#define EINTMOD1_EINT5CON(x)	(x<<2)
#define EINTMOD1_EINT4CON(x)	(x<<0)

#define EINT7CON_RISE		(0x1)
#define EINT7CON_FALL		(0x2)
#define EINT7CON_BOTH		(0x3)
#define EINT6CON_RISE		(0x1)
#define EINT6CON_FALL		(0x2)
#define EINT6CON_BOTH		(0x3)
#define EINT5CON_RISE		(0x1)
#define EINT5CON_FALL		(0x2)
#define EINT5CON_BOTH		(0x3)
#define EINT4CON_RISE		(0x1)
#define EINT4CON_FALL		(0x2)
#define EINT4CON_BOTH		(0x3)

#define EINTMOD1_EINT4CON_RISE	(EINTMOD1_EINT4CON(EINT4CON_RISE))
#define EINTMOD1_EINT4CON_FALL	(EINTMOD1_EINT4CON(EINT4CON_FALL))
#define EINTMOD1_EINT4CON_BOTH	(EINTMOD1_EINT4CON(EINT4CON_BOTH))
#define EINTMOD1_EINT5CON_RISE	(EINTMOD1_EINT5CON(EINT5CON_RISE))
#define EINTMOD1_EINT5CON_FALL	(EINTMOD1_EINT5CON(EINT5CON_FALL))
#define EINTMOD1_EINT5CON_BOTH	(EINTMOD1_EINT5CON(EINT5CON_BOTH))
#define EINTMOD1_EINT6CON_RISE	(EINTMOD1_EINT6CON(EINT6CON_RISE))
#define EINTMOD1_EINT6CON_FALL	(EINTMOD1_EINT6CON(EINT6CON_FALL))
#define EINTMOD1_EINT6CON_BOTH	(EINTMOD1_EINT6CON(EINT6CON_BOTH))
#define EINTMOD1_EINT7CON_RISE	(EINTMOD1_EINT7CON(EINT7CON_RISE))
#define EINTMOD1_EINT7CON_FALL	(EINTMOD1_EINT7CON(EINT7CON_FALL))
#define EINTMOD1_EINT7CON_BOTH	(EINTMOD1_EINT7CON(EINT7CON_BOTH))

// 0xFE45 rEINTEN0			// External Interrupt Enable Register 0
#define EINTEN0_EINT7		(0x80)	// Enable external Interrupt 7
#define EINTEN0_EINT6		(0x40)	// Enable external Interrupt 6
#define EINTEN0_EINT5		(0x20)	// Enable external Interrupt 5
#define EINTEN0_EINT4		(0x10)	// Enable external Interrupt 4
#define EINTEN0_EINT3		(0x08)	// Enable external Interrupt 3
#define EINTEN0_EINT2		(0x04)	// Enable external Interrupt 2
#define EINTEN0_EINT1		(0x02)	// Enable external Interrupt 1
#define EINTEN0_EINT0		(0x01)	// Enable external Interrupt 0

// 0xFE47 rEINTPND0			// External Interrupt Pendign Register 0
#define EINTPND0_EINT7		(0x80)	// Enable external Interrupt 7
#define EINTPND0_EINT6		(0x40)	// Enable external Interrupt 6
#define EINTPND0_EINT5		(0x20)	// Enable external Interrupt 5
#define EINTPND0_EINT4		(0x10)	// Enable external Interrupt 4
#define EINTPND0_EINT3		(0x08)	// Enable external Interrupt 3
#define EINTPND0_EINT2		(0x04)	// Enable external Interrupt 2
#define EINTPND0_EINT1		(0x02)	// Enable external Interrupt 1
#define EINTPND0_EINT0		(0x01)	// Enable external Interrupt 0

// 0xFE49 rP0PUR 			// P00 ~ P07 Pullup control Register
#define P0PUR_PUEN0			(0x01)
#define P0PUR_PUEN1			(0x02)
#define P0PUR_PUEN2			(0x04)
#define P0PUR_PUEN3			(0x08)
#define P0PUR_PUEN4			(0x10)
#define P0PUR_PUEN5			(0x20)
#define P0PUR_PUEN6			(0x40)
#define P0PUR_PUEN7			(0x80)

#define PxPUR_DISABLE		(0x00)
#define PxPUR_ENABLE		(0xFF)

// 0xFE4A rP1PUR 			// P10 ~ P17 Pullup control Register
#define P1PUR_PUEN0			(0x01)
#define P1PUR_PUEN1			(0x02)
#define P1PUR_PUEN2			(0x04)
#define P1PUR_PUEN3			(0x08)
#define P1PUR_PUEN4			(0x10)
#define P1PUR_PUEN5			(0x20)
#define P1PUR_PUEN6			(0x40)
#define P1PUR_PUEN7			(0x80)

// 0xFE4B rP3PUR	 		// P30 ~ P33 Pullup/down control Register
#define P3PUR_PUEN0			(0x01)
#define P3PUR_PUEN1			(0x02)
#define P3PUR_PUEN2			(0x04)
#define P3PUR_PUEN3			(0x08)
#define P3PUR_PUEN4			(0x10)
#define P3PUR_PUEN5			(0x20)
#define P3PUR_PUEN6			(0x40)
#define P3PUR_PUEN7			(0x80)

// 0xFE4C rP4PUR	 		// P40 ~ P47 Pullup/down control Register
#define P4PUR_PUEN0			(0x01)
#define P4PUR_PUEN1			(0x02)
#define P4PUR_PUEN2			(0x04)
#define P4PUR_PUEN3			(0x08)
#define P4PUR_PUEN4			(0x10)
#define P4PUR_PUEN5			(0x20)
#define P4PUR_PUEN6			(0x40)
#define P4PUR_PUEN7			(0x80)

// 0xFE4D rP5PUR 			// P50 ~ P57 Pullup control Register
#define P5PUR_PUEN0			(0x01)
#define P5PUR_PUEN1			(0x02)
#define P5PUR_PUEN2			(0x04)
#define P5PUR_PUEN3			(0x08)
#define P5PUR_PUEN4			(0x10)
#define P5PUR_PUEN5			(0x20)
#define P5PUR_PUEN6			(0x40)
#define P5PUR_PUEN7			(0x80)

// 0xFE4E rP6PUR 			// P60 ~ P67 Pullup control Register
#define P6PUR_PUEN0			(0x01)
#define P6PUR_PUEN1			(0x02)
#define P6PUR_PUEN2			(0x04)
#define P6PUR_PUEN3			(0x08)
#define P6PUR_PUEN4			(0x10)
#define P6PUR_PUEN5			(0x20)
#define P6PUR_PUEN6			(0x40)
#define P6PUR_PUEN7			(0x80)

// 0xFE4F rP7PUR 			// P70 ~ P72 Pullup control Register
#define P7PUR_PUEN0			(0x01)
#define P7PUR_PUEN1			(0x02)
#define P7PUR_PUEN2			(0x04)

#define GP0MD_MASK			(0x0F)
#define GP1MD_MASK			(0xF0)
#define GP2MD_MASK			(0x0F)
#define GP3MD_MASK			(0xF0)
#define GP4MD_MASK			(0x0F)
#define GP5MD_MASK			(0xF0)
#define GP6MD_MASK			(0x0F)
#define GP7MD_MASK			(0xF0)

// 0xFE50 rP0MOD0 			// P00 ~ P01 mode control Register
#define P0MOD0_GP01MD(x)		(x<<4)	// P01 Mode Control Bit
#define P0MOD0_GP00MD(x)		(x<<0)	// P00 Mode Control Bit

// 0xFE51 rP0MOD1 			// P02 ~ P03 mode control Register
#define P0MOD1_GP03MD(x)		(x<<4)	// P03 Mode Control Bit
#define P0MOD1_GP02MD(x)		(x<<0)	// P02 Mode Control Bit

// 0xFE52 rP0MOD2 			// P04 ~ P05 mode control Register
#define P0MOD2_GP05MD(x)		(x<<4)	// P05 Mode Control Bit
#define P0MOD2_GP04MD(x)		(x<<0)	// P04 Mode Control Bit

// 0xFE53 rP0MOD3 			// P06 ~ P07 mode control Register
#define P0MOD3_GP07MD(x)		(x<<4)	// P07 Mode Control Bit
#define P0MOD3_GP06MD(x)		(x<<0)	// P06 Mode Control Bit

// 0xFE54 rP1MOD0 			// P10 ~ P11 mode control Register
#define P1MOD0_GP11MD(x)		(x<<4)	// P11 Mode Control Bit
#define P1MOD0_GP10MD(x)		(x<<0)	// P10 Mode Control Bit

// 0xFE55 rP1MOD1 			// P12 ~ P13 mode control Register
#define P1MOD1_GP13MD(x)		(x<<4)	// P13 Mode Control Bit
#define P1MOD1_GP12MD(x)		(x<<0)	// P12 Mode Control Bit

// 0xFE56 rP1MOD2 			// P14 ~ P15 mode control Register
#define P1MOD2_GP15MD(x)		(x<<4)	// P15 Mode Control Bit
#define P1MOD2_GP14MD(x)		(x<<0)	// P14 Mode Control Bit

// 0xFE57 rP1MOD3 			// P16 ~ P17 mode control Register
#define P1MOD3_GP17MD(x)		(x<<4)	// P17 Mode Control Bit
#define P1MOD3_GP16MD(x)		(x<<0)	// P16 Mode Control Bit

// 0xFE58 rP3MOD0	 		// P30 ~ P31 mode control Register
#define P3MOD0_GP31MD(x)		(x<<4)	// P31 Mode Control Bit
#define P3MOD0_GP30MD(x)		(x<<0)	// P30 Mode Control Bit
// 0xFE59 rP3MOD1 			// P32 ~ P33 mode control Register
#define P3MOD1_GP33MD(x)		(x<<4)	// P33 Mode Control Bit
#define P3MOD1_GP32MD(x)		(x<<0)	// P32 Mode Control Bit
// 0xFE5A rP3MOD2 			// P34 ~ P35 mode control Register
#define P3MOD2_GP35MD(x)		(x<<4)	// P35 Mode Control Bit
#define P3MOD2_GP34MD(x)		(x<<0)	// P34 Mode Control Bit
// 0xFE5B rP3MOD3 			// P36 ~ P37 mode control Register
#define P3MOD3_GP37MD(x)		(x<<4)	// P37 Mode Control Bit
#define P3MOD3_GP36MD(x)		(x<<0)	// P36 Mode Control Bit

// 0xFE5C rP4MOD0 			// P40 ~ P41 mode control Register
#define P4MOD0_GP41MD(x)		(x<<4)	// P41 Mode Control Bit
#define P4MOD0_GP40MD(x)		(x<<0)	// P40 Mode Control Bit
// 0xFE5D rP4MOD1 			// P42 ~ P43 mode control Register
#define P4MOD1_GP43MD(x)		(x<<4)	// P43 Mode Control Bit
#define P4MOD1_GP42MD(x)		(x<<0)	// P42 Mode Control Bit
// 0xFE5E rP4MOD2 			// P44 ~ P45 mode control Register
#define P4MOD2_GP45MD(x)		(x<<4)	// P45 Mode Control Bit
#define P4MOD2_GP44MD(x)		(x<<0)	// P44 Mode Control Bit
// 0xFE5F rP4MOD3 			// P46 ~ P47 mode control Register
#define P4MOD3_GP47MD(x)		(x<<4)	// P47 Mode Control Bit
#define P4MOD3_GP46MD(x)		(x<<0)	// P46 Mode Control Bit

// 0xFE60 rP5MOD0 			// P50 ~ P51 mode control Register
#define P5MOD0_GP51MD(x)		(x<<4)	// P51 Mode Control Bit
#define P5MOD0_GP50MD(x)		(x<<0)	// P50 Mode Control Bit
// 0xFE61 rP5MOD1 			// P52 ~ P53 mode control Register
#define P5MOD1_GP53MD(x)		(x<<4)	// P53 Mode Control Bit
#define P5MOD1_GP52MD(x)		(x<<0)	// P52 Mode Control Bit
// 0xFE62 rP5MOD2 			// P54 ~ P55 mode control Register
#define P5MOD2_GP55MD(x)		(x<<4)	// P55 Mode Control Bit
#define P5MOD2_GP54MD(x)		(x<<0)	// P54 Mode Control Bit
// 0xFE63 rP5MOD3 			// P56 ~ P57 mode control Register
#define P5MOD3_GP57MD(x)		(x<<4)	// P57 Mode Control Bit
#define P5MOD3_GP56MD(x)		(x<<0)	// P56 Mode Control Bit

// 0xFE64 rP6MOD0 			// P60 ~ P61 mode control Register
#define P6MOD0_GP61MD(x)		(x<<4)	// P61 Mode Control Bit
#define P6MOD0_GP60MD(x)		(x<<0)	// P60 Mode Control Bit
// 0xFE65 rP6MOD1 			// P62 ~ P63 mode control Register
#define P6MOD1_GP63MD(x)		(x<<4)	// P63 Mode Control Bit
#define P6MOD1_GP62MD(x)		(x<<0)	// P62 Mode Control Bit
// 0xFE66 rP6MOD2 			// P64 ~ P66 mode control Register
#define P6MOD2_GP65MD(x)		(x<<4)	// P65 Mode Control Bit
#define P6MOD2_GP64MD(x)		(x<<0)	// P64 Mode Control Bit
// 0xFE67 rP6MOD3 			// P66 ~ P67 mode control Register
#define P6MOD3_GP67MD(x)		(x<<4)	// P67 Mode Control Bit
#define P6MOD3_GP66MD(x)		(x<<0)	// P66 Mode Control Bit

// 0xFE68 rP7MOD0 			// P70 ~ P71 mode control Register
#define P7MOD0_GP71MD(x)		(x<<4)	// P71 Mode Control Bit
#define P7MOD0_GP70MD(x)		(x<<0)	// P70 Mode Control Bit
// 0xFE69 rP7MOD1 			// P72 mode control Register
#define P7MOD1_GP72MD(x)		(x<<0)	// P72 Mode Control Bit

#define HIGHZ			(0x0)	// High-Z
#define INPUT			(0x1)	// Data Input Mode
#define OUTPUT			(0x2)	// Data Output Mode
#define FUNC0			(0x3)
#define FUNC1			(0x4)
#define FUNC2			(0x5)

#define GP00_EINT0		(0x4)
#define GP00_BUZ		(0x3)

#define GP01_KEY2		(0x5)
#define GP01_EINT1		(0x4)
#define GP01_REM		(0x3)

#define GP02_SCSN		(0x3)
#define GP02_EINT2		(0x4)

#define GP03_SMOSI		(0x3)
#define GP03_EINT3		(0x4)

#define GP04_SMISO		(0x3)
#define GP04_EINT4		(0x4)

#define GP05_SCLK		(0x3)
#define GP05_EINT5		(0x4)

#define GP06_UTXD		(0x3)
#define GP06_EINT6		(0x4)

#define GP07_URXD		(0x3)
#define GP07_EINT7		(0x4)

#define GP10_SEG0		(0x3)
#define GP10_EINT0		(0x4)

#define GP11_SEG1		(0x3)
#define GP11_EINT1		(0x4)

#define GP12_SEG2		(0x3)
#define GP12_EINT2		(0x4)

#define GP13_SEG3		(0x3)
#define GP13_EINT3		(0x4)

#define GP14_SEG4		(0x3)
#define GP14_EINT4		(0x4)

#define GP15_SEG5		(0x3)
#define GP15_EINT5		(0x4)

#define GP16_SEG6		(0x3)
#define GP16_EINT6		(0x4)

#define GP17_SEG7		(0x3)
#define GP17_EINT7		(0x4)

#define GP30_TCK		(0x3)

#define GP31_TMS		(0x3)

#define GP32_TDI		(0x3)

#define GP33_TDO		(0x3)

#define GP34_COM0		(0x3)
#define GP34_EINT4		(0x4)

#define GP35_COM1		(0x3)
#define GP35_EINT5		(0x4)

#define GP36_COM2		(0x3)
#define GP36_EINT6		(0x4)

#define GP37_COM3		(0x3)
#define GP37_EINT7		(0x4)

#define GP40_SEG8		(0x3)
#define GP40_EINT0		(0x4)

#define GP41_SEG9		(0x3)
#define GP41_EINT1		(0x4)

#define GP42_SEG10		(0x3)
#define GP42_EINT2		(0x4)

#define GP43_SEG11		(0x3)
#define GP43_EINT3		(0x4)

#define GP44_SEG12		(0x3)
#define GP44_EINT4		(0x4)

#define GP45_SEG13		(0x3)
#define GP45_EINT5		(0x4)

#define GP46_SEG14		(0x3)
#define GP46_EINT6		(0x4)

#define GP47_SEG15		(0x3)
#define GP47_EINT7		(0x4)

#define GP50_SEG16		(0x3)

#define GP51_SEG17		(0x3)

#define GP52_SEG18		(0x3)
#define GP52_SCSN		(0x4)

#define GP53_SEG19		(0x3)
#define GP53_SMOSI		(0x4)

#define GP54_SEG20		(0x3)
#define GP54_SMISO		(0x4)

#define GP55_SEG21		(0x3)
#define GP55_SCLK		(0x4)

#define GP56_SEG22		(0x3)
#define GP56_UTXD		(0x4)

#define GP57_SEG23		(0x3)
#define GP57_URXD		(0x4)

#define GP60_SEG24		(0x3)

#define GP61_SEG25		(0x3)

#define GP62_SEG26		(0x3)
#define GP62_EINT2		(0x4)

#define GP63_SEG27		(0x3)
#define GP63_TCLK		(0x4)

#define GP64_SEG28		(0x3)
#define GP64_T0OUTCAP	(0x4)

#define GP65_SEG29		(0x3)
#define GP65_T1OUTCAP	(0x4)

#define GP66_SEG30		(0x3)
#define GP66_T2OUTCAP	(0x4)

#define GP67_SEG31		(0x3)
#define GP67_EINT7		(0x4)

#define GP70_T3OUTCAP	(0x3)
#define GP70_EINT0		(0x4)
#define GP70_KEY0		(0x5)

#define GP71_NRSTO		(0x3)
#define GP71_EINT1		(0x4)
#define GP71_KEY1		(0x5)

#define GP72_CLKO		(0x3)
#define GP72_VBLD		(0x4)

#define P0MOD0_GP00MD_HIGHZ	(P0MOD0_GP00MD(HIGHZ))
#define P0MOD0_GP00MD_INPUT	(P0MOD0_GP00MD(INPUT))
#define P0MOD0_GP00MD_OUTPUT	(P0MOD0_GP00MD(OUTPUT))
#define P0MOD0_GP00MD_BUZ	(P0MOD0_GP00MD(GP00_BUZ))
#define P0MOD0_GP00MD_EINT0	(P0MOD0_GP00MD(GP00_EINT0))
#define P0MOD0_GP01MD_HIGHZ	(P0MOD0_GP01MD(HIGHZ))
#define P0MOD0_GP01MD_INPUT	(P0MOD0_GP01MD(INPUT))
#define P0MOD0_GP01MD_OUTPUT	(P0MOD0_GP01MD(OUTPUT))
#define P0MOD0_GP01MD_REM	(P0MOD0_GP01MD(GP01_REM))
#define P0MOD0_GP01MD_EINT1	(P0MOD0_GP01MD(GP01_EINT1))
#define P0MOD0_GP01MD_KEY2	(P0MOD0_GP01MD(GP01_KEY2))

#define P0MOD1_GP02MD_HIGHZ	(P0MOD1_GP02MD(HIGHZ))
#define P0MOD1_GP02MD_INPUT	(P0MOD1_GP02MD(INPUT))
#define P0MOD1_GP02MD_OUTPUT	(P0MOD1_GP02MD(OUTPUT))
#define P0MOD1_GP02MD_SCSN	(P0MOD1_GP02MD(GP02_SCSN))
#define P0MOD1_GP02MD_EINT2	(P0MOD1_GP02MD(GP02_EINT2))
#define P0MOD1_GP03MD_HIGHZ	(P0MOD1_GP03MD(HIGHZ))
#define P0MOD1_GP03MD_INPUT	(P0MOD1_GP03MD(INPUT))
#define P0MOD1_GP03MD_OUTPUT	(P0MOD1_GP03MD(OUTPUT))
#define P0MOD1_GP03MD_SMOSI	(P0MOD1_GP03MD(GP03_SMOSI))
#define P0MOD1_GP03MD_EINT3	(P0MOD1_GP03MD(GP03_EINT3))

#define P0MOD2_GP04MD_HIGHZ	(P0MOD2_GP04MD(HIGHZ))
#define P0MOD2_GP04MD_INPUT	(P0MOD2_GP04MD(INPUT))
#define P0MOD2_GP04MD_OUTPUT	(P0MOD2_GP04MD(OUTPUT))
#define P0MOD2_GP04MD_SMISO	(P0MOD2_GP04MD(GP04_SMISO))
#define P0MOD2_GP04MD_EINT4	(P0MOD2_GP04MD(GP04_EINT4))
#define P0MOD2_GP05MD_HIGHZ	(P0MOD2_GP05MD(HIGHZ))
#define P0MOD2_GP05MD_INPUT	(P0MOD2_GP05MD(INPUT))
#define P0MOD2_GP05MD_OUTPUT	(P0MOD2_GP05MD(OUTPUT))
#define P0MOD2_GP05MD_SCLK	(P0MOD2_GP05MD(GP05_SCLK))
#define P0MOD2_GP05MD_EINT5	(P0MOD2_GP05MD(GP05_EINT5))

#define P0MOD3_GP06MD_HIGHZ	(P0MOD3_GP06MD(HIGHZ))
#define P0MOD3_GP06MD_INPUT	(P0MOD3_GP06MD(INPUT))
#define P0MOD3_GP06MD_OUTPUT	(P0MOD3_GP06MD(OUTPUT))
#define P0MOD3_GP06MD_UTXD	(P0MOD3_GP06MD(GP06_UTXD))
#define P0MOD3_GP06MD_EINT6	(P0MOD3_GP06MD(GP06_EINT6))
#define P0MOD3_GP07MD_HIGHZ	(P0MOD3_GP07MD(HIGHZ))
#define P0MOD3_GP07MD_INPUT	(P0MOD3_GP07MD(INPUT))
#define P0MOD3_GP07MD_OUTPUT	(P0MOD3_GP07MD(OUTPUT))
#define P0MOD3_GP07MD_URXD	(P0MOD3_GP07MD(GP07_URXD))
#define P0MOD3_GP07MD_EINT7	(P0MOD3_GP07MD(GP07_EINT7))

#define P1MOD0_GP10MD_HIGHZ	(P1MOD0_GP10MD(HIGHZ))
#define P1MOD0_GP10MD_INPUT	(P1MOD0_GP10MD(INPUT))
#define P1MOD0_GP10MD_OUTPUT	(P1MOD0_GP10MD(OUTPUT))
#define P1MOD0_GP10MD_SEG0	(P1MOD0_GP10MD(GP10_SEG0))
#define P1MOD0_GP10MD_EINT0	(P1MOD0_GP10MD(GP10_EINT0))
#define P1MOD0_GP11MD_HIGHZ	(P1MOD0_GP11MD(HIGHZ))
#define P1MOD0_GP11MD_INPUT	(P1MOD0_GP11MD(INPUT))
#define P1MOD0_GP11MD_OUTPUT	(P1MOD0_GP11MD(OUTPUT))
#define P1MOD0_GP11MD_SEG1	(P1MOD0_GP11MD(GP11_SEG1))
#define P1MOD0_GP11MD_EINT1	(P1MOD0_GP11MD(GP11_EINT1))

#define P1MOD1_GP12MD_HIGHZ	(P1MOD1_GP12MD(HIGHZ))
#define P1MOD1_GP12MD_INPUT	(P1MOD1_GP12MD(INPUT))
#define P1MOD1_GP12MD_OUTPUT	(P1MOD1_GP12MD(OUTPUT))
#define P1MOD1_GP12MD_SEG2	(P1MOD1_GP12MD(GP12_SEG2))
#define P1MOD1_GP12MD_EINT2	(P1MOD1_GP12MD(GP12_EINT2))
#define P1MOD1_GP13MD_HIGHZ	(P1MOD1_GP13MD(HIGHZ))
#define P1MOD1_GP13MD_INPUT	(P1MOD1_GP13MD(INPUT))
#define P1MOD1_GP13MD_OUTPUT	(P1MOD1_GP13MD(OUTPUT))
#define P1MOD1_GP13MD_SEG3	(P1MOD1_GP13MD(GP13_SEG3))
#define P1MOD1_GP13MD_EINT3	(P1MOD1_GP13MD(GP13_EINT3))

#define P1MOD2_GP14MD_HIGHZ	(P1MOD2_GP14MD(HIGHZ))
#define P1MOD2_GP14MD_INPUT	(P1MOD2_GP14MD(INPUT))
#define P1MOD2_GP14MD_OUTPUT	(P1MOD2_GP14MD(OUTPUT))
#define P1MOD2_GP14MD_SEG4	(P1MOD2_GP14MD(GP14_SEG4))
#define P1MOD2_GP14MD_EINT4	(P1MOD2_GP14MD(GP14_EINT4))
#define P1MOD2_GP15MD_HIGHZ	(P1MOD2_GP15MD(HIGHZ))
#define P1MOD2_GP15MD_INPUT	(P1MOD2_GP15MD(INPUT))
#define P1MOD2_GP15MD_OUTPUT	(P1MOD2_GP15MD(OUTPUT))
#define P1MOD2_GP15MD_SEG5	(P1MOD2_GP15MD(GP15_SEG5))
#define P1MOD2_GP15MD_EINT5	(P1MOD2_GP15MD(GP15_EINT5))

#define P1MOD3_GP16MD_HIGHZ	(P1MOD3_GP16MD(HIGHZ))
#define P1MOD3_GP16MD_INPUT	(P1MOD3_GP16MD(INPUT))
#define P1MOD3_GP16MD_OUTPUT	(P1MOD3_GP16MD(OUTPUT))
#define P1MOD3_GP16MD_SEG6	(P1MOD3_GP16MD(GP16_SEG6))
#define P1MOD3_GP16MD_EINT6	(P1MOD3_GP16MD(GP16_EINT6))
#define P1MOD3_GP17MD_HIGHZ	(P1MOD3_GP17MD(HIGHZ))
#define P1MOD3_GP17MD_INPUT	(P1MOD3_GP17MD(INPUT))
#define P1MOD3_GP17MD_OUTPUT	(P1MOD3_GP17MD(OUTPUT))
#define P1MOD3_GP17MD_SEG7	(P1MOD3_GP17MD(GP17_SEG7))
#define P1MOD3_GP17MD_EINT7	(P1MOD3_GP17MD(GP17_EINT7))

#define P3MOD0_GP30MD_HIGHZ	(P3MOD0_GP30MD(HIGHZ))
#define P3MOD0_GP30MD_INPUT	(P3MOD0_GP30MD(INPUT))
#define P3MOD0_GP30MD_OUTPUT	(P3MOD0_GP30MD(OUTPUT))
#define P3MOD0_GP30MD_TCK	(P3MOD0_GP30MD(GP30_TCK))
#define P3MOD0_GP31MD_HIGHZ	(P3MOD0_GP31MD(HIGHZ))
#define P3MOD0_GP31MD_INPUT	(P3MOD0_GP31MD(INPUT))
#define P3MOD0_GP31MD_OUTPUT	(P3MOD0_GP31MD(OUTPUT))
#define P3MOD0_GP31MD_TMS	(P3MOD0_GP31MD(GP31_TMS))

#define P3MOD1_GP32MD_HIGHZ	(P3MOD1_GP32MD(HIGHZ))
#define P3MOD1_GP32MD_INPUT	(P3MOD1_GP32MD(INPUT))
#define P3MOD1_GP32MD_OUTPUT	(P3MOD1_GP32MD(OUTPUT))
#define P3MOD1_GP32MD_TDI	(P3MOD1_GP32MD(GP32_TDI))
#define P3MOD1_GP33MD_HIGHZ	(P3MOD1_GP33MD(HIGHZ))
#define P3MOD1_GP33MD_INPUT	(P3MOD1_GP33MD(INPUT))
#define P3MOD1_GP33MD_OUTPUT	(P3MOD1_GP33MD(OUTPUT))
#define P3MOD1_GP33MD_TDO	(P3MOD1_GP33MD(GP33_TDO))

#define P3MOD2_GP34MD_HIGHZ	(P3MOD2_GP34MD(HIGHZ))
#define P3MOD2_GP34MD_INPUT	(P3MOD2_GP34MD(INPUT))
#define P3MOD2_GP34MD_OUTPUT	(P3MOD2_GP34MD(OUTPUT))
#define P3MOD2_GP34MD_COM0	(P3MOD2_GP34MD(GP34_COM0))
#define P3MOD2_GP34MD_EINT4	(P3MOD2_GP34MD(GP34_EINT4))
#define P3MOD2_GP35MD_HIGHZ	(P3MOD2_GP35MD(HIGHZ))
#define P3MOD2_GP35MD_INPUT	(P3MOD2_GP35MD(INPUT))
#define P3MOD2_GP35MD_OUTPUT	(P3MOD2_GP35MD(OUTPUT))
#define P3MOD2_GP35MD_COM1	(P3MOD2_GP35MD(GP35_COM1))
#define P3MOD2_GP35MD_EINT5	(P3MOD2_GP35MD(GP35_EINT5))

#define P3MOD3_GP36MD_HIGHZ	(P3MOD3_GP36MD(HIGHZ))
#define P3MOD3_GP36MD_INPUT	(P3MOD3_GP36MD(INPUT))
#define P3MOD3_GP36MD_OUTPUT	(P3MOD3_GP36MD(OUTPUT))
#define P3MOD3_GP36MD_COM2	(P3MOD3_GP36MD(GP36_COM2))
#define P3MOD3_GP36MD_EINT6	(P3MOD3_GP36MD(GP36_EINT6))
#define P3MOD3_GP37MD_HIGHZ	(P3MOD3_GP37MD(HIGHZ))
#define P3MOD3_GP37MD_INPUT	(P3MOD3_GP37MD(INPUT))
#define P3MOD3_GP37MD_OUTPUT	(P3MOD3_GP37MD(OUTPUT))
#define P3MOD3_GP37MD_COM3	(P3MOD3_GP37MD(GP37_COM3))
#define P3MOD3_GP37MD_EINT7	(P3MOD3_GP37MD(GP37_EINT7))

#define P4MOD0_GP40MD_HIGHZ	(P4MOD0_GP40MD(HIGHZ))
#define P4MOD0_GP40MD_INPUT	(P4MOD0_GP40MD(INPUT))
#define P4MOD0_GP40MD_OUTPUT	(P4MOD0_GP40MD(OUTPUT))
#define P4MOD0_GP40MD_SEG8	(P4MOD0_GP40MD(GP40_SEG8))
#define P4MOD0_GP40MD_EINT0	(P4MOD0_GP40MD(GP40_EINT0))
#define P4MOD0_GP41MD_HIGHZ	(P4MOD0_GP41MD(HIGHZ))
#define P4MOD0_GP41MD_INPUT	(P4MOD0_GP41MD(INPUT))
#define P4MOD0_GP41MD_OUTPUT	(P4MOD0_GP41MD(OUTPUT))
#define P4MOD0_GP41MD_SEG9	(P4MOD0_GP41MD(GP41_SEG9))
#define P4MOD0_GP41MD_EINT1	(P4MOD0_GP41MD(GP41_EINT1))

#define P4MOD1_GP42MD_HIGHZ	(P4MOD1_GP42MD(HIGHZ))
#define P4MOD1_GP42MD_INPUT	(P4MOD1_GP42MD(INPUT))
#define P4MOD1_GP42MD_OUTPUT	(P4MOD1_GP42MD(OUTPUT))
#define P4MOD1_GP42MD_SEG10	(P4MOD1_GP42MD(GP42_SEG10))
#define P4MOD1_GP42MD_EINT2	(P4MOD1_GP42MD(GP42_EINT2))
#define P4MOD1_GP43MD_HIGHZ	(P4MOD1_GP43MD(HIGHZ))
#define P4MOD1_GP43MD_INPUT	(P4MOD1_GP43MD(INPUT))
#define P4MOD1_GP43MD_OUTPUT	(P4MOD1_GP43MD(OUTPUT))
#define P4MOD1_GP43MD_SEG11	(P4MOD1_GP43MD(GP43_SEG11))
#define P4MOD1_GP43MD_EINT3	(P4MOD1_GP43MD(GP43_EINT3))

#define P4MOD2_GP44MD_HIGHZ	(P4MOD2_GP44MD(HIGHZ))
#define P4MOD2_GP44MD_INPUT	(P4MOD2_GP44MD(INPUT))
#define P4MOD2_GP44MD_OUTPUT	(P4MOD2_GP44MD(OUTPUT))
#define P4MOD2_GP44MD_SEG12	(P4MOD2_GP44MD(GP44_SEG12))
#define P4MOD2_GP44MD_EINT4	(P4MOD2_GP44MD(GP44_EINT4))
#define P4MOD2_GP45MD_HIGHZ	(P4MOD2_GP45MD(HIGHZ))
#define P4MOD2_GP45MD_INPUT	(P4MOD2_GP45MD(INPUT))
#define P4MOD2_GP45MD_OUTPUT	(P4MOD2_GP45MD(OUTPUT))
#define P4MOD2_GP45MD_SEG13	(P4MOD2_GP45MD(GP45_SEG13))
#define P4MOD2_GP45MD_EINT5	(P4MOD2_GP45MD(GP45_EINT5))

#define P4MOD3_GP46MD_HIGHZ	(P4MOD3_GP46MD(HIGHZ))
#define P4MOD3_GP46MD_INPUT	(P4MOD3_GP46MD(INPUT))
#define P4MOD3_GP46MD_OUTPUT	(P4MOD3_GP46MD(OUTPUT))
#define P4MOD3_GP46MD_SEG14	(P4MOD3_GP46MD(GP46_SEG14))
#define P4MOD3_GP46MD_EINT6	(P4MOD3_GP46MD(GP46_EINT6))
#define P4MOD3_GP47MD_HIGHZ	(P4MOD3_GP47MD(HIGHZ))
#define P4MOD3_GP47MD_INPUT	(P4MOD3_GP47MD(INPUT))
#define P4MOD3_GP47MD_OUTPUT	(P4MOD3_GP47MD(OUTPUT))
#define P4MOD3_GP47MD_SEG15	(P4MOD3_GP47MD(GP47_SEG15))
#define P4MOD3_GP47MD_EINT7	(P4MOD3_GP47MD(GP47_EINT7))

#define P5MOD0_GP50MD_HIGHZ	(P5MOD0_GP50MD(HIGHZ))
#define P5MOD0_GP50MD_INPUT	(P5MOD0_GP50MD(INPUT))
#define P5MOD0_GP50MD_OUTPUT	(P5MOD0_GP50MD(OUTPUT))
#define P5MOD0_GP50MD_SEG16	(P5MOD0_GP50MD(GP50_SEG16))
#define P5MOD0_GP51MD_HIGHZ	(P5MOD0_GP51MD(HIGHZ))
#define P5MOD0_GP51MD_INPUT	(P5MOD0_GP51MD(INPUT))
#define P5MOD0_GP51MD_OUTPUT	(P5MOD0_GP51MD(OUTPUT))
#define P5MOD0_GP51MD_SEG17	(P5MOD0_GP51MD(GP51_SEG17))

#define P5MOD1_GP52MD_HIGHZ	(P5MOD1_GP52MD(HIGHZ))
#define P5MOD1_GP52MD_INPUT	(P5MOD1_GP52MD(INPUT))
#define P5MOD1_GP52MD_OUTPUT	(P5MOD1_GP52MD(OUTPUT))
#define P5MOD1_GP52MD_SEG18	(P5MOD1_GP52MD(GP52_SEG18))
#define P5MOD1_GP52MD_SCSN	(P5MOD1_GP52MD(GP52_SCSN))
#define P5MOD1_GP53MD_HIGHZ	(P5MOD1_GP53MD(HIGHZ))
#define P5MOD1_GP53MD_INPUT	(P5MOD1_GP53MD(INPUT))
#define P5MOD1_GP53MD_OUTPUT	(P5MOD1_GP53MD(OUTPUT))
#define P5MOD1_GP53MD_SEG19	(P5MOD1_GP53MD(GP53_SEG19))
#define P5MOD1_GP53MD_SMOSI	(P5MOD1_GP53MD(GP53_SMOSI))

#define P5MOD2_GP54MD_HIGHZ	(P5MOD2_GP54MD(HIGHZ))
#define P5MOD2_GP54MD_INPUT	(P5MOD2_GP54MD(INPUT))
#define P5MOD2_GP54MD_OUTPUT	(P5MOD2_GP54MD(OUTPUT))
#define P5MOD2_GP54MD_SEG20	(P5MOD2_GP54MD(GP54_SEG20))
#define P5MOD2_GP54MD_SMISO	(P5MOD2_GP54MD(GP54_SMISO))
#define P5MOD2_GP55MD_HIGHZ	(P5MOD2_GP55MD(HIGHZ))
#define P5MOD2_GP55MD_INPUT	(P5MOD2_GP55MD(INPUT))
#define P5MOD2_GP55MD_OUTPUT	(P5MOD2_GP55MD(OUTPUT))
#define P5MOD2_GP55MD_SEG21	(P5MOD2_GP55MD(GP55_SEG21))
#define P5MOD2_GP55MD_SCLK	(P5MOD2_GP55MD(GP55_SCLK))

#define P5MOD3_GP56MD_HIGHZ	(P5MOD3_GP56MD(HIGHZ))
#define P5MOD3_GP56MD_INPUT	(P5MOD3_GP56MD(INPUT))
#define P5MOD3_GP56MD_OUTPUT	(P5MOD3_GP56MD(OUTPUT))
#define P5MOD3_GP56MD_SEG22	(P5MOD3_GP56MD(GP56_SEG22))
#define P5MOD3_GP56MD_UTXD	(P5MOD3_GP56MD(GP56_UTXD))
#define P5MOD3_GP57MD_HIGHZ	(P5MOD3_GP57MD(HIGHZ))
#define P5MOD3_GP57MD_INPUT	(P5MOD3_GP57MD(INPUT))
#define P5MOD3_GP57MD_OUTPUT	(P5MOD3_GP57MD(OUTPUT))
#define P5MOD3_GP57MD_SEG23	(P5MOD3_GP57MD(GP57_SEG23))
#define P5MOD3_GP57MD_URXD	(P5MOD3_GP57MD(GP57_URXD))

#define P6MOD0_GP60MD_HIGHZ	(P6MOD0_GP60MD(HIGHZ))
#define P6MOD0_GP60MD_INPUT	(P6MOD0_GP60MD(INPUT))
#define P6MOD0_GP60MD_OUTPUT	(P6MOD0_GP60MD(OUTPUT))
#define P6MOD0_GP60MD_SEG24	(P6MOD0_GP60MD(GP60_SEG24))
#define P6MOD0_GP61MD_HIGHZ	(P6MOD0_GP61MD(HIGHZ))
#define P6MOD0_GP61MD_INPUT	(P6MOD0_GP61MD(INPUT))
#define P6MOD0_GP61MD_OUTPUT	(P6MOD0_GP61MD(OUTPUT))
#define P6MOD0_GP61MD_SEG25	(P6MOD0_GP61MD(GP61_SEG25))

#define P6MOD1_GP62MD_HIGHZ	(P6MOD1_GP62MD(HIGHZ))
#define P6MOD1_GP62MD_INPUT	(P6MOD1_GP62MD(INPUT))
#define P6MOD1_GP62MD_OUTPUT	(P6MOD1_GP62MD(OUTPUT))
#define P6MOD1_GP62MD_SEG26	(P6MOD1_GP62MD(GP62_SEG26))
#define P6MOD1_GP62MD_EINT2	(P6MOD1_GP62MD(GP62_EINT2))
#define P6MOD1_GP63MD_HIGHZ	(P6MOD1_GP63MD(HIGHZ))
#define P6MOD1_GP63MD_INPUT	(P6MOD1_GP63MD(INPUT))
#define P6MOD1_GP63MD_OUTPUT	(P6MOD1_GP63MD(OUTPUT))
#define P6MOD1_GP63MD_SEG27	(P6MOD1_GP63MD(GP63_SEG27))
#define P6MOD1_GP63MD_TCLK	(P6MOD1_GP63MD(GP63_TCLK))

#define P6MOD2_GP64MD_HIGHZ	(P6MOD2_GP64MD(HIGHZ))
#define P6MOD2_GP64MD_INPUT	(P6MOD2_GP64MD(INPUT))
#define P6MOD2_GP64MD_OUTPUT	(P6MOD2_GP64MD(OUTPUT))
#define P6MOD2_GP64MD_SEG28	(P6MOD2_GP64MD(GP64_SEG28))
#define P6MOD2_GP64MD_T0OUTCAP	(P6MOD2_GP64MD(GP64_T0OUTCAP))
#define P6MOD2_GP65MD_HIGHZ	(P6MOD2_GP65MD(HIGHZ))
#define P6MOD2_GP65MD_INPUT	(P6MOD2_GP65MD(INPUT))
#define P6MOD2_GP65MD_OUTPUT	(P6MOD2_GP65MD(OUTPUT))
#define P6MOD2_GP65MD_SEG29	(P6MOD2_GP65MD(GP65_SEG29))
#define P6MOD2_GP65MD_T1OUTCAP	(P6MOD2_GP65MD(GP65_T1OUTCAP))

#define P6MOD3_GP66MD_HIGHZ	(P6MOD3_GP66MD(HIGHZ))
#define P6MOD3_GP66MD_INPUT	(P6MOD3_GP66MD(INPUT))
#define P6MOD3_GP66MD_OUTPUT	(P6MOD3_GP66MD(OUTPUT))
#define P6MOD3_GP66MD_SEG30	(P6MOD3_GP66MD(GP66_SEG30))
#define P6MOD3_GP66MD_T2OUTCAP	(P6MOD3_GP66MD(GP66_T2OUTCAP))
#define P6MOD3_GP67MD_HIGHZ	(P6MOD3_GP67MD(HIGHZ))
#define P6MOD3_GP67MD_INPUT	(P6MOD3_GP67MD(INPUT))
#define P6MOD3_GP67MD_OUTPUT	(P6MOD3_GP67MD(OUTPUT))
#define P6MOD3_GP67MD_SEG31	(P6MOD3_GP67MD(GP67_SEG31))
#define P6MOD3_GP67MD_EINT7	(P6MOD3_GP67MD(GP67_EINT7))

#define P7MOD0_GP70MD_HIGHZ	(P7MOD0_GP70MD(HIGHZ))
#define P7MOD0_GP70MD_INPUT	(P7MOD0_GP70MD(INPUT))
#define P7MOD0_GP70MD_OUTPUT	(P7MOD0_GP70MD(OUTPUT))
#define P7MOD0_GP70MD_T3OUTCAP	(P7MOD0_GP70MD(GP70_T3OUTCAP))
#define P7MOD0_GP70MD_EINT0	(P7MOD0_GP70MD(GP70_EINT0))
#define P7MOD0_GP70MD_KEY0	(P7MOD0_GP70MD(GP70_KEY0))
#define P7MOD0_GP71MD_HIGHZ	(P7MOD0_GP71MD(HIGHZ))
#define P7MOD0_GP71MD_INPUT	(P7MOD0_GP71MD(INPUT))
#define P7MOD0_GP71MD_OUTPUT	(P7MOD0_GP71MD(OUTPUT))
#define P7MOD0_GP71MD_NRSTO	(P7MOD0_GP71MD(GP71_NRSTO))
#define P7MOD0_GP71MD_EINT1	(P7MOD0_GP71MD(GP71_EINT1))
#define P7MOD0_GP71MD_KEY1	(P7MOD0_GP71MD(GP71_KEY1))

#define P7MOD1_GP72MD_HIGHZ	(P7MOD1_GP72MD(HIGHZ))
#define P7MOD1_GP72MD_INPUT	(P7MOD1_GP72MD(INPUT))
#define P7MOD1_GP72MD_OUTPUT	(P7MOD1_GP72MD(OUTPUT))
#define P7MOD1_GP72MD_CLKO	(P7MOD1_GP72MD(GP72_CLKO))
#define P7MOD1_GP72MD_VBLD	(P7MOD1_GP72MD(GP72_VBLD))


// -----------------------------------------------------------------------
// LCD
// -----------------------------------------------------------------------
// 0xFFC0 rLCON				; LCD Control Register
#define LCON_BIAS(x)		(x<<3)
#define LCON_DUTY(x)		(x<<1)
#define LCON_ENABLE			(0x01)

#define BIAS_12			(0x0)
#define BIAS_13			(0x1)
#define DUTY_12			(0x0)
#define DUTY_13			(0x1)
#define DUTY_14			(0x2)

#define LCON_BIAS_12		(LCON_BIAS(BIAS_12))
#define LCON_BIAS_13		(LCON_BIAS(BIAS_13))
#define LCON_DUTY_12		(LCON_DUTY(DUTY_12))
#define LCON_DUTY_13		(LCON_DUTY(DUTY_13))
#define LCON_DUTY_14		(LCON_DUTY(DUTY_14))

// 0xFFC1 rLMOD				; LCD Mode Register
#define LMOD_SEGCTRL(x)		(x<<4)
#define LMOD_RESSIZE(x)		(x<<2)
#define LMOD_DYNAMIC		(0x02)
#define LMOD_MODE			(0x01)

#define SEGCTRL_NORMAL		(0x0)
#define SEGCTRL_OFF			(0x1)
#define SEGCTRL_ON			(0x2)

#define RESSIZE_175K		(0x0)
#define RESSIZE_75K			(0x1)
#define RESSIZE_25K			(0x2)

#define LMOD_SEGCTRL_NORMAL	(0x00)
#define LMOD_SEGCTRL_OFF		(0x10)
#define LMOD_SEGCTRL_ON		(0x20)

#define LMOD_RESSIZE_175K	(0x00)
#define LMOD_RESSIZE_75K		(0x04)
#define LMOD_RESSIZE_25K		(0x08)

#define LMOD_MODE_FRAME		(0x00)
#define LMOD_MODE_LINE		(0x01)

// 0xFFC2 rLCKSEL				; LCD Clock Select Register
#define LCKSEL_SRCSEL(x)		(x<<7)
#define LCKSEL_CLKSEL(x)		(x<<0)

#define SRCSEL_WT			(0x0)
#define SRCSEL_RTC			(0x1)

#define CLKSEL_WT2048HZ		(0x0)
#define CLKSEL_WT1024HZ		(0x1)
#define CLKSEL_WT512HZ		(0x2)
#define CLKSEL_WT256HZ		(0x3)
#define CLKSEL_WT128HZ		(0x4)
#define CLKSEL_WT64HZ		(0x5)
#define CLKSEL_WT32HZ		(0x6)
#define CLKSEL_WT16HZ		(0x7)

#define CLKSEL_RTC2048HZ		(0x7)
#define CLKSEL_RTC1024HZ		(0x6)
#define CLKSEL_RTC512HZ		(0x5)
#define CLKSEL_RTC256HZ		(0x4)
#define CLKSEL_RTC128HZ		(0x3)
#define CLKSEL_RTC64HZ		(0x2)
#define CLKSEL_RTC32HZ		(0x1)
#define CLKSEL_RTC16HZ		(0x0)

#define LCKSEL_SRCSEL_WT		(0x00)
#define LCKSEL_SRCSEL_RTC	(0x80)

#define LCKSEL_CLKSEL_WT2048HZ	(0x0)
#define LCKSEL_CLKSEL_WT1024HZ	(0x1)
#define LCKSEL_CLKSEL_WT512HZ		(0x2)
#define LCKSEL_CLKSEL_WT256HZ		(0x3)
#define LCKSEL_CLKSEL_WT128HZ		(0x4)
#define LCKSEL_CLKSEL_WT64HZ		(0x5)
#define LCKSEL_CLKSEL_WT32HZ		(0x6)
#define LCKSEL_CLKSEL_WT16HZ		(0x7)

#define LCKSEL_CLKSEL_RTC2048HZ	(LCKSEL_CLKSEL_WT16HZ)
#define LCKSEL_CLKSEL_RTC1024HZ	(LCKSEL_CLKSEL_WT32HZ)
#define LCKSEL_CLKSEL_RTC512HZ	(LCKSEL_CLKSEL_WT64HZ)
#define LCKSEL_CLKSEL_RTC256HZ	(LCKSEL_CLKSEL_WT128HZ)
#define LCKSEL_CLKSEL_RTC128HZ	(LCKSEL_CLKSEL_WT256HZ)
#define LCKSEL_CLKSEL_RTC64HZ		(LCKSEL_CLKSEL_WT512HZ)
#define LCKSEL_CLKSEL_RTC32HZ		(LCKSEL_CLKSEL_WT1024HZ)
#define LCKSEL_CLKSEL_RTC16HZ		(LCKSEL_CLKSEL_WT2048HZ)

// 0xFFC3 rLCONTR				; LCD Contrast Control Register
#define LCONTR_CONTR(x)			(x<<0)

// 0xFFC4 rLCONTRDYN			; LCD Contrast Control Register
#define LCONTRDYN_CONTRDYN(x)		(x<<0)


// -----------------------------------------------------------------------
// FLASH
// -----------------------------------------------------------------------

// 0xFFA0 rFLASHCTRL			// Flash Control Register
#define FLASHCTRL_FPDDISSUBOP		(0x80)
#define FLASHCTRL_SCPENBSUBOP		(0x40)
#define FLASHCTRL_PWUP8MHZ		(0x20)
#define FLASHCTRL_BWPMODE(x)		(x<<3)
#define FLASHCTRL_OPRMODE(x)		(x<<1)
#define FLASHCTRL_CHIPERASE		(0x01)

#define BWPMODE_PROGRAM			(0x0)
#define BWPMODE_DMA				(0x1)
#define BWPMODE_LOAD			(0x2)
#define OPRMODE_ERASE			(0x0)
#define OPRMODE_PROGRAM			(0x2)

#define FLASHCTRL_BWPMODE_PROGRAM	(0x00)
#define FLASHCTRL_BWPMODE_DMA		(0x08)
#define FLASHCTRL_BWPMODE_LOAD	(0x10)

#define FLASHCTRL_OPRMODE_ERASE	(0x00)
#define FLASHCTRL_OPRMODE_PROGRAM	(0x02)

// 0xFFA1 rFLASHSTAT
#define FLASHSTAT_BUSY			(0x01)

// 0xFFA2 rFLASHADDR0
// 0xFFA3 rFLASHADDR1
// 0xFFA4 rFLASHADDR2

// 0xFFA6 rFLASHWDATA

// 0xFFA8 rFLASHWP				// Program/Sector_Erase Protect Register
#define FLASHWP_WP(x)			(x<<0)
#define WP_AREA0				(0x01)
#define WP_AREA1				(0x02)
#define WP_AREA2				(0x04)
#define WP_AREA3				(0x08)
#define WP_AREA4				(0x10)
#define WP_AREA5				(0x20)
#define WP_AREA6				(0x40)
#define WP_AREA7				(0x80)

// 0xFFAA rXRAMADDR0
// 0xFFAB rXRAMADDR1
// 0xFFAC rFLASHTC
#define FLASHTC_TC_MASK			(0x3F)

//============================================================================
//      Definitions for Extended SFR Registers (SFR16)
//============================================================================

#endif	/* __IAR_SYSTEM_ICC__ */



#endif	/* __BMC285_REGDEF_h__ */

//==========================================================================
//       Revision History
//==========================================================================
// Rev:   Date:                Reason:
