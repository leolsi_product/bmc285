//===================================================================
// File Name : SysInit.c
// Function  : System Initialization
// Program   : Boo-Ree Multimedia Inc.
// Date      : February, 04, 2013
// Version   : 0.01
// Mail      : support@boo-ree.com
// Web       : www.boo-ree.com
// History
//===================================================================

#define __SYSINIT_c__
#include "includes.h"
//#include "test.h"

/*
 * Initialize System control
 */
__near_func void System_Init(void)
{
	// Watch-dog Timer(WDT) disable.
	WDT_DISABLE;

	GlobalIntDisable;

	//
	// System clock initialize.
	//
#if (Fsys==12000000)	// EMCLK
//	rSMCLKCON = SMCLKCON_FLASHWAITCNT(FLASHWAITCNT_0); // IM 8MHz
	rSMCLKCON = SMCLKCON_FLASHWAITCNT(FLASHWAITCNT_1); // EM 12MHz
	rSYSCFG = 0x0;							// IM/EM/IS/ES Oscillator Enable.
	while(!(rSYSCFG & SYSCFG_EMSTABLE)); 	//Wait for PXI Stable

	// Fsys = EMCLK(12MHz)
	rCLKCON = CLKCON_CLKO(CLKO_FSYS) | CLKCON_CLKDIV(CLKDIV_1) | CLKCON_CLKSRC(CLKSRC_EMCLK);
#elif (Fsys==8000000)	// IMCLK
//	rSMCLKCON = SMCLKCON_FLASHWAITCNT(FLASHWAITCNT_0); // IM 8MHz
	rSYSCFG = SYSCFG_EMDIS | SYSCFG_ESDIS;
	// Fsys = IMCLK(8MHz)
	rCLKCON = CLKCON_CLKO(CLKO_FSYS) | CLKCON_CLKDIV(CLKDIV_1) | CLKCON_CLKSRC(CLKSRC_IMCLK);
#endif	
	rP7MOD1 = P7MOD1_GP72MD(GP72_CLKO);		// Clock Output Port Settup.

	// Enable all system clock
//	rCLKEN0 = CLKEN0_FLASH;
}


#if 0
#include <math.h>

/* Baud Rate Clock = (System Clock) / (Fixed Point Number CKDIV[15:0] * 16)
 *     - Baud Rate clock -> 115200-bps : 115200 * 16 = 1843200 Hz
 *     - System clock -> 12 Mhz
 *     - division ratio = Fsys / Fbaus = 12M / 1843200 = 6.51041666
 *          -> integer part : 6(3-bits), CDIBW = 2(3-bits)
 *          -> 정수는 6이고, 3-bits로 표시되므로, CKDIV[15:13]에 정수를 입력한다.
 *             즉, CKDIV[15:13] = 0x6 이다.
 *          -> 소수 부분은 CKDIV register의 16-bits중에서 3-bits는 정수를 표시하고, 
 *             나머지 13-bits는 소수부분을 표시한다.
 *             즉, 0.51041666 * 2^13 = 4181.3333 이고, 
 *             여기서 정수 부분만 CKDIV[12:0]에 write한다.(CKDIV[12:0] = 4181(0x1055))
 *          -> 오차률은 계산은 아래와 같다.
 *             4181 / 2^13 = 0.5103759765625이고, 정수와 합쳐서 12Mhz / (6.5103759765625 / 16) = 115200.720004500028125...이다.0.000006% 오차.
 *
 *     - Baud Rate clock -> 57600-bps : 57600 * 16 = 921600 Hz
 *     - System clock -> 12 Mhz
 *     - division ratio = Fsys / Fbaus = 12M / 921600 = 13.020833
 *          -> integer part : 13(4-bits), CDIBW = 3(4-bits)
 *          -> 정수는 13이고, 4-bits로 표시되므로, CKDIV[15:12]에 정수를 입력한다.
 *             즉, CKDIV[15:12] = 13 이다.
 *          -> 소수 부분은 CKDIV register의 16-bits중에서 3-bits는 정수를 표시하고, 
 *             나머지 13-bits는 소수부분을 표시한다.
 *             즉, 0.020833 * 2^12 = 85.3333 이고, 
 *             여기서 정수 부분만 CKDIV[11:0]에 write한다.(CKDIV[11:0] = 85(0x055))
 *             => CKDIV[15:0] = 0xD000 + 0x055 = 0xD055
 *
 *          -> Fsys / Fbaus = 7Mhz / (X * 16) = 13.020833
 *             X = 7Mhz / (13.020833 * 16) = 33600.00086 Hz -> 1 / 29.76us
 *
 *          -> 정수 13은 4-bit로 표시됨, CKDIV[15:12]에 정수를 입력함.
 *             CKDIV[15:12] = 13(0xD), CKDIV[11:0] = 85(0x55)
 */
int get_int_1_pos(int i)
{
    int bitPos = 0x8000;
	int n = 16;

    while (1)
    {
        if (i & bitPos)
            break;
        bitPos >>= 1;
        n--;
    }

    return n;
}

int make_ClkDiv(uint16_t iBaudRate)
{
	uint32_t clk = 12 * MHZ;//get_clock();
	float cd;
	double fractpart, intpart;
	int ibw;

	/* Baud Rate
	 * Baud Rate = (system clock) / (16 * CD)
	 *
	 * ex) CD = 20Mhz / (16 * 57600) = 21.701
	 */
	cd = clk / (16. * iBaudRate);
	fractpart = modf((double)cd, &intpart);
	ibw = get_int_1_pos((int)intpart);
	intpart = ldexp(intpart, 16 - ibw);
	fractpart = ldexp(fractpart, 16 - ibw);

	/* Set Baudrate */
	rU0CON1 = CDIBW(ibw - 1);
	rU0CKDIV = (int)(intpart + fractpart);

	return (int)(intpart + fractpart);
}
#endif

/*
 * Initialize UART communication port
 *
 * Argument: byBaud, ibw
 *
 */
__near_func void Console_Init(uint16_t byBaud, uint8_t ibw)
{
	// UART GPIO Initialzie.
#if 1
	rP0MOD3 = P0MOD3_GP07MD(GP07_URXD) | P0MOD3_GP06MD(GP06_UTXD);
	rP0PUR = P0PUR_PUEN6 | P0PUR_PUEN7;
#else
	rP5MOD3 = P5MOD3_GP57MD(GP57_URXD) | P5MOD3_GP56MD(GP56_UTXD);
	rP5PUR = P5PUR_PUEN6 | P5PUR_PUEN7;
#endif

//	make_ClkDiv(57600);

	rCLKEN0 |= CLKEN0_UART0;

	rU0CON0  = U0CON0_WL(WL_8) | U0CON0_PMD(PMD_NO);	// data 8-bit, no parity, stop 1-bit
	rU0CON1  = U0CON1_CDIBW(ibw);
	rU0CKDIV16 = byBaud;
	rU0CMD   = U0CMD_TXEN | U0CMD_RXEN | U0CMD_RFFCLR | U0CMD_TFFCLR;
	rU0IPND  = U0IPND_ALL;					// Pending clear.
}

/*
 * Initialize GPIO Port
 */
__near_func void GPIO_Init(void)
{
	// All Port High-Z mode, pull-up disable.
	rP0PUR = rP1PUR = rP3PUR = rP4PUR = rP5PUR = rP6PUR = rP7PUR = PxPUR_DISABLE;

	rP0MOD0 = rP0MOD1 = rP0MOD2 = rP0MOD3 = P0MOD0_GP00MD(HIGHZ) | P0MOD0_GP01MD(HIGHZ);
	rP1MOD0 = rP1MOD1 = rP1MOD2 = rP1MOD3 = P1MOD0_GP10MD(HIGHZ) | P1MOD0_GP11MD(HIGHZ);
#if 0
	rJTAGOFF = 0x1;
	rP3MOD0 = rP3MOD1 = P3MOD0_GP30MD(HIGHZ) | P3MOD0_GP31MD(HIGHZ);
#endif
	rP3MOD2 = rP3MOD3 = P3MOD0_GP30MD(HIGHZ) | P3MOD0_GP31MD(HIGHZ);
	rP4MOD0 = rP4MOD1 = rP4MOD2 = rP4MOD3 = P4MOD0_GP40MD(HIGHZ) | P4MOD0_GP41MD(HIGHZ);
	rP5MOD0 = rP5MOD1 = rP5MOD2 = rP5MOD3 = P5MOD0_GP50MD(HIGHZ) | P5MOD0_GP51MD(HIGHZ);
	rP6MOD0 = rP6MOD1 = rP6MOD2 = rP6MOD3 = P6MOD0_GP60MD(HIGHZ) | P6MOD0_GP61MD(HIGHZ);
	rP7MOD0 = rP7MOD1 = P7MOD0_GP70MD(HIGHZ) | P7MOD0_GP71MD(HIGHZ);
}

__near_func void LED_Init(uint8_t led)
{
	// GP6.4, GP6.5
	if( led & LED0 ) rP6MOD2 = (rP6MOD2 & ~GP4MD_MASK) | P6MOD2_GP64MD_OUTPUT;
	if( led & LED1 ) rP6MOD2 = (rP6MOD2 & ~GP5MD_MASK) | P6MOD2_GP65MD_OUTPUT;
}

__near_func void Timer_Init(uint8_t ch)
{
	/* TnCLK = Fsys / 2^(CLK_SEL+1), Fsys = 12Mhz or 8MHz
	 *    when CLK_SEL = 0 : TnCLK = Fsys / 2^1 = Fsys / 2 = 8/12MHz / 2 = 4/6MHz
	 *    when CLK_SEL = 1 : TnCLK = Fsys / 2^2 = Fsys / 4 = 8/12MHz / 4 = 2/3MHz
	 *    when CLK_SEL = 2 : TnCLK = Fsys / 2^3 = Fsys / 8 = 8/12MHz / 8 = 1/1.5MHz
	 *    when CLK_SEL = 3 : TnCLK = Fsys / 2^4 = Fsys / 16 = 8/12MHz / 16 = 500/750KHz
	 *    when CLK_SEL = 4 : TnCLK = Fsys / 2^5 = Fsys / 32 = 8/12MHz / 32 = 250/325KHz
	 *    when CLK_SEL = 5 : TnCLK = Fsys / 2^6 = Fsys / 64 = 8/12MHz / 64 = 125/162.5KHz
	 * TnOUT = TnCNT / TnCLK, TnCNT = TnOUT * TnCLK
	 *    Fsys = 8Mhz, 1ms Time-out : Tcnt = 1ms * 4MHz = 1 * 4 * 10^3 = 4000
	 *                                Tcnt = 1ms * 500KHz = 1 * 500 = 500
	 *                                Tcnt = 1ms * 250KHz = 1 * 250 = 250
	 *    Fsys = 12Mhz, 1ms Time-out : Tcnt = 1ms * 162.5KHz = 1 * 162.5 = 162
	 */
	if( ch == 0 ) {
		// Timer-0 Run : 1ms tick.
		rCLKEN0 |= CLKEN0_TIMER01;				// Timer0/1 Clock Enable

		// Timer0 clock = Fsys/(2^4) = 250KHz
#if (Fsys==12000000)	// EMCLK
		rT0DATA  = 162;						// 1-ms time-out.
		rT0STAT  = TnSTAT_INTPND | TnSTAT_MATCH;
		rT0CON   = TnCON_CLKSEL(5) | TnCON_MODSEL_INTERVAL | TnCON_RUN_START;
#elif (Fsys==8000000)	// IMCLK
		rT0DATA  = 250;						// 1-ms time-out.
		rT0STAT  = TnSTAT_INTPND | TnSTAT_MATCH;
		rT0CON   = TnCON_CLKSEL(4) | TnCON_MODSEL_INTERVAL | TnCON_RUN_START;
#endif
		rIE0     |= IE0_TIMER0;		// Timer0 Match Interrupt enable

		tick_1ms = 0;
	} else {
	}
}

__near_func void SystemInitialization(void)
{
	System_Init();				// SysInit.c
	GPIO_Init();				// SysInit.c
	LED_Init(LED0|LED1);		// SysInit.c
	Timer_Init(0);				// SysInit.c
	Console_Init(BAUD_57600, IBW_57600);	// SysInit.c
}

