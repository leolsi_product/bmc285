//===================================================================
// File Name : System.c
// Function  : Misc Function's Implementation
// Program   : Boo-Ree Multimedia Inc.
// Date      : February, 04, 2013
// Mail      : support@boo-ree.com
// Web       : www.boo-ree.com
// History
//===================================================================

#define __SYSTEM_c__
#include "includes.h"

__near_func void _10udelay(uint8_t us)
{
	uint16_t i;

	for(i=0; i<us; i++) ;
}

__idata uint8_t start, diff, tick, cnt, end;
__near_func void mdelay(uint16_t ms)
{
	cnt = ms / 200;
	end = ms % 200;

	while( cnt ) {
		start = tick_1ms;
//		printf((char *)"cnt = %d, end = %d, start = %d\n", cnt, end, start);
		do {
			tick = tick_1ms;
			if( tick < start ) diff = ((255 - start) + tick);
			else  diff = tick - start;
		} while( diff < 200 );
		cnt--;
	}

	start = tick_1ms;
	do {
		tick = tick_1ms;
		if( tick < start ) diff = ((255 - start) + tick);
		else  diff = tick - start;
	} while( diff < end );
}

__near_func void delay(uint8_t sec)
{
	do {
		mdelay(1000);
	} while( sec-- );
}

__near_func void LED_TOGGLE(uint8_t led)
{
	if( led & LED0 ) LED0_PORT ^= 1;
	if( led & LED1 ) LED1_PORT ^= 1;
}

__near_func void LED_ON(uint8_t led)
{
	if( led & LED0 ) LED0_PORT = 1;
	if( led & LED1 ) LED1_PORT = 1;
}

__near_func void LED_OFF(uint8_t led)
{
	if( led & LED0 ) LED0_PORT = 0;
	if( led & LED1 ) LED1_PORT = 0;
}

void putch(int ch)
{
	if (ch == '\n') {
		rU0BUF = '\r';
		while (!(rU0TXSTAT & U0TXSTAT_TXI)) ;
	}

	rU0BUF = ch;
	while (!(rU0TXSTAT & U0TXSTAT_TXI)) ;
}

int getkey(void)
{
	uint8_t error;

	if( rU0IPND & U0IPND_ERROR ) {
		error = rU0EXSTAT;
		rU0EXSTAT &= ~error;
		if( error & U0EXSTAT_OVF ) {
			rU0BUF;
		} else if( error & (U0EXSTAT_UNDR | U0EXSTAT_PER | U0EXSTAT_FER | U0EXSTAT_BKD) ) {
//			rU0EXSTAT &= ~(U0EXSTAT_UNDR | U0EXSTAT_PER | U0EXSTAT_FER | U0EXSTAT_BKD);
		}
		rU0IPND = U0IPND_ERROR;
	} else if( rU0RXSTAT & U0RXSTAT_RDV )
		return rU0BUF;

	return -1;
}

#if (__CODE_MODEL__ == 2)
__near_func
#endif
int putchar(int ch)
{
	putch(ch);

	return ch;
}

#if (__CODE_MODEL__ == 2)
__near_func
#endif
int getchar(void)
{
	int ch = 0;

	do {
		ch = getkey();
	} while( ch == -1 );

	return ch;
}

void putdigit(uint8_t val, uint8_t cr)
{
	uint8_t t;

	t = (val / 10) % 10;
	putchar(t+'0');
	t = (val / 1) % 10;
	putchar(t+'0');

	if( cr ) putchar(cr);
}

// max : 255
void putint(uint8_t val, uint8_t cr)
{
	uint8_t t, z = 0;

	t = (val / 100) % 10;
	if( t ) {
		putchar(t+'0');
		z = 1;
	}
	t = (val / 10) % 10;
	if( t || z ) putchar(t+'0');
	t = (val / 1) % 10;
	putchar(t+'0');

	if( cr ) putchar(cr);
}

// max : 65535
void putint16(uint16_t val, uint8_t cr)
{
	uint8_t t, z=0;
	uint16_t div;

	div = 10000;
	do {
		t = (val / div) % 10;

		if( t || z || (div==1)) {
			putchar(t+'0');
			z = 1;
		}
		div = div / 10;
	} while( div );

	if( cr ) putchar(cr);
}

// max : 4294967295
void putint32(uint32_t val, uint8_t cr)
{
	uint8_t t, z = 0;
	uint32_t div;

	div = 1000000000;
	do {
		t = (val / div) % 10;

		if( t || z ) {
			putchar(t+'0');
			z = 1;
		}
		div = div / 10;
	} while( div );

	if( cr ) putchar(cr);
}


void puthex(uint8_t val, uint8_t cr)
{
	uint8_t t, d = 8;

	do {
		t = (val >> (d-4)) & 0xF;
		if( t < 10 ) t += '0';
		else t += ('A' - 10);
		putchar(t);
		d -= 4;
	} while( d != 0 );

	if( cr ) putchar(cr);
}

void puthex16(uint16_t val, uint8_t cr)
{
	uint8_t t, d = 16;

	do {
		t = (val >> (d - 4)) & 0xF;
		if( t < 10 ) t += '0';
		else t += ('A' - 10);
		putchar(t);
		d -= 4;
	} while( d != 0 );

	if( cr ) putchar(cr);
}

void puthex32(uint32_t val, uint8_t cr)
{
	uint8_t t, d = 32;

	do {
		t = (val >> (d - 4)) & 0xF;
		if( t < 10 ) t += '0';
		else t += ('A' - 10);
		putchar(t);
		d -= 4;
	} while( d != 0 );

	if( cr ) putchar(cr);
}

void putstr(const char __code* str)
{
	while( *str ) putchar(*str++);
}

void dump(uint8_t *ptr, uint8_t size)
{
	uint8_t i = 0;

	do {
		if( !(i & 0xF) ) {
			putstr("0x"); puthex16((uint16_t)ptr, ' '); putstr(": ");
		}
		putstr("0x"); puthex(*ptr++, 0); putchar(' ');
		i++;
		if( !(i & 0xF) ) putchar('\n');
	} while( --size );
}

