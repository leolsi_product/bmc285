//===================================================================
// File Name : main.c
// Function  : Primary Main Function's Implementation
// Program   : Boo-Ree Multimedia Inc.
// Date      : February, 04, 2013
// Version   : 0.01
// Mail      : support@boo-ree.com
// Web       : www.boo-ree.com
// History
//===================================================================

//===================================================================
//	Instruction to RUN !!!!!!
//	 1) Build -> Build (F7)
//	 2) Debug -> Start Debugging (F5)
//	 3) RUN (Ctrl+F6)
//===================================================================

#define __MAIN_c__
#include "includes.h"

__no_init __idata volatile uint8_t loop;

// interrupt disable function.
__monitor uint8_t check_tick(uint8_t task_tick)
{
	if( tick_1ms < task_tick ) return ((255 - task_tick) + tick_1ms);
	else return (tick_1ms - task_tick);
}

__task void main(void)
{

	SystemInitialization();		// SysInit.c 

	//GlobalIntEnable;
	__enable_interrupt();

	printf((char *)"\n\n\n=========================================\n");
	printf((char *)"         [ %s Simple Code ]\n", ((rPCON & 0xFC)==0x30)?"BMC285":"BMC288");
	printf((char *)" - SYSTEM CLOCK : %d Mhz\n", Fsys/MHZ);
	printf((char *)" - UART : 57600-bps, 8n1\n");
	printf((char *)" - RESET STATUS : 0x%x\n", rRSTSTAT);
	printf((char *)"=========================================\n\n");

	do {
		mdelay(500);
		printf((char *)"%s() : %d Test Function!\n", __func__, loop++);
		LED_TOGGLE(LED0);
#if 0
		if( check_tick(tx_tick) >= 250 ) {			// 250 ms
			tx_tick = tick_10ms;
			printf((char *)"%s() : Test Function!\n", __func__);
			LED_TOGGLE(LED0);
		}
#endif
		rWDTCON |= WDTCON_CNTCLR;
	} while(getkey() != ESC);
}




//#pragma register_bank=3
#pragma vector=0x03
__interrupt void _WDTInt()							// 0
{
	rWDTCON |= WDTCON_INTPEND;
}

#pragma vector=0x0b
__interrupt void _UartInt()							// 1
{
}

#pragma vector=0x13
__interrupt void _RTCInt()							// 2
{
	rRTCIPND = rRTCIPND;

	rRTCINTBS = rRTCINTBS;
}

#pragma vector=0x1b
__interrupt void _WTInt()							// 3
{
	rWTCON |= WTCON_INTPND;
}

#pragma vector=0x23
__interrupt void _ExInt0()							// 4
{
	rEINTPND0 = EINTPND0_EINT0;
}

#pragma vector=0x2B
__interrupt void _ExInt1()							// 5
{
	rEINTPND0 = EINTPND0_EINT1;
}

#pragma vector=0x33
__interrupt void _SPIInt()							// 6
{
	rSPICK |= SPICK_INTPEND;
}

#pragma vector=0x3B
__interrupt void _TimerInt0()						// 7
{
	rT0STAT |= TnSTAT_INTPND;
	tick_1ms++;
}

#pragma vector=0x43
__interrupt void _TimerInt2()						// 8
{
	rT2STAT |= TnSTAT_INTPND;
}

#pragma vector=0x4B
__interrupt void _KdtInt()							// 9
{
	rKDTCON |= KDTCON_INTPND;
}

#pragma vector=0x53
__interrupt void _TimerInt1()						// 10
{
	rT1STAT |= TnSTAT_INTPND;
}

#pragma vector=0x5B
__interrupt void _CAFGInt()							// 11
{
	rCACON0 |= CACON0_INTPND;
}

#pragma vector=0x63
__interrupt void _TimerInt3()						// 12
{
	rT3STAT |= TnSTAT_INTPND;
}

#pragma vector=0x6B
__interrupt void _ExInt2()							// 13
{
	rEINTPND0 = EINTPND0_EINT2;
}


#pragma vector=0x73
__interrupt void _ExInt3()							// 14
{
	rEINTPND0 = EINTPND0_EINT3;
}


#pragma vector=0x7B
__interrupt void _ExInt4()							// 15
{
	rEINTPND0 = EINTPND0_EINT4;
}


#pragma vector=0x83
__interrupt void _ExInt5()							// 16
{
	rEINTPND0 = EINTPND0_EINT5;
}


#pragma vector=0x8B
__interrupt void _ExInt6()							// 17
{
	rEINTPND0 = EINTPND0_EINT6;
}


#pragma vector=0x93
__interrupt void _ExInt7()							// 18
{
	rEINTPND0 = EINTPND0_EINT7;
}

